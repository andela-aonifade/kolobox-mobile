import * as constants from '../constants';

// action creator stuff below
export const asyncRequest = request => ({
  type: constants.ASYNC_REQUEST,
  request
})

export const asyncReceiveSuccess = request => ({
  type: constants.ASYNC_RECEIVE_SUCCESS,
  request
})

export const asyncReceiveError = request => ({
  type: constants.ASYNC_RECEIVE_ERROR,
  request
})
