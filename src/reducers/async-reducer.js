import {
  asyncRequest,
  asyncReceiveSuccess,
  asyncReceiveError
} from '../actions/async-actions';

import {
  ASYNC_REQUEST,
  ASYNC_RECEIVE_ERROR,
  ASYNC_RECEIVE_SUCCESS
} from '../constants';

// reducer stuff below
const initialState = {}

export default (state = initialState, action) => {
  const { request } = action || {}

  switch (action.type) {
    case ASYNC_REQUEST:
      return {
        ...state,
        [request.type]: {
          ...request,
          async: true
        }
      }
    case ASYNC_RECEIVE_SUCCESS:
      return {
        ...state,
        [request.type]: {
          ...request,
          async: false,
          success: true
        }
      }
    case ASYNC_RECEIVE_ERROR:
      return {
        ...state,
        [request.type]: {
          ...request,
          async: false,
          success: true
        }
      }
    default:
      return state
  }
}
