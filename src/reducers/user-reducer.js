import {
  SET_USER_DATA,
  SET_FIRST_TIME_USER,
  SET_TOKEN,
  GET_USER_DASHBOARD,
  GET_USER_EARNINGS,
  GET_USER_OVERALL_EARNINGS
} from '../constants';

const initialState = {
  token: '',
  userData: {},
  firstTimeUser: true,
  earnings: {},
  overall_earnings: {},
  dashboard: {
    savings: {
      data: {
        total_savings: null,
        average_savings: null
      },
      chart: []
    },
    interest: {
      data: [],
      chart: []
    }
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_DATA: return _setUser(state, action)
    case SET_FIRST_TIME_USER: return _setFirstTime(state, action)
    case SET_TOKEN: return _setToken(state, action)
    case GET_USER_DASHBOARD: return _setDashboard(state, action)
    case GET_USER_EARNINGS: return _setEarnings(state, action)
    case GET_USER_OVERALL_EARNINGS: return _setOverallEarnings(state, action)
    default:
      return state
  }
}

const _setUser = (state, action) => {
  const { userData } = action;
  return {
    ...state,
    userData
  }
}

const _setFirstTime = (state, action) => {
  const { firstTimeUser } = action
  return {
    ...state,
    firstTimeUser
  }
}

const _setToken = (state, action) => {
  const { token } = action
  return {
    ...state,
    token
  }
}

const _setDashboard = (state, action) => {
  const { dashboard } = action
  return {
    ...state,
    dashboard
  }
}

const _setEarnings = (state, action) => {
  const { earnings } = action
  return {
    ...state,
    earnings
  }
}

const _setOverallEarnings = (state, action) => {
  const { overall_earnings } = action
  return {
    ...state,
    overall_earnings
  }
}
