import { ADD_TOAST } from '../constants'

export default function toast (state = null, action) {
  switch (action.type) {
    case ADD_TOAST:
      return action.message
    default:
      return state
  }
}
