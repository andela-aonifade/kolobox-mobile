import GLOBALS from '../globals'

/* global fetch */

/**
 * @param {string} oldPin
 * @param {string} newPin
 */
export const UPDATE_PIN = (token, oldPin, newPin) => {
  const url = `${GLOBALS.BASE_URL}user/me/pin`
  return fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify({old_pin: oldPin, new_pin: newPin})
  })
.then(response => response.json())
}

/**
 * authenticate a user
 * @param {string} username
 * @param {string} password
 */
export const LOGIN = (username, password) => {
  const url = `${GLOBALS.BASE_URL}auth/user/login`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({username, password})
  })
  .then(response => response.json())
}

export const SIGNUP = (formObject) => {
  const url = `${GLOBALS.BASE_URL}auth/user/create`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(formObject)
  })
  .then(response => response.json())
}

/**
 * verify the email address if it's been used previously for registration
 * @param {string} email
 */
export const VERIFY_EMAIL = email => {
  const url = `${GLOBALS.BASE_URL}helpers/verifyEmail?email=${email}`
  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(response => response.json())
}

export const VERIFY_PHONE = (phone) => {
  const url = `${GLOBALS.BASE_URL}helpers/verifyPhone?phone=${phone}`
  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(response => response.json())
}

/**
 * @param {string} token
 * @param {string} code
 */
export const VERIFY_PIN = (token, code) => {
  const url = `${GLOBALS.BASE_URL}auth/user/validate/code`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify({code})
  })
}

/**
 * @param {string} token
 * @param {string} code
 */
export const RESEND_PIN = (token, emailPhone) => {
  const url = `${GLOBALS.BASE_URL}auth/user/resend/confirmation_code`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify({emailPhone})
  })
  .then(response => response.json())
}

/**
 * set a pin for the user
 * @param {string} token
 * @param {string} pin
 */
export const SET_USER_PIN = (token, pin) => {
  const url = `${GLOBALS.BASE_URL}user/me/pin`
  pin = +pin
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify({pin})
  })
  .then(response => response.json())
}

/**
 * loads the logged in user profile information
 * @param {string} token
 */
export const GET_USER_PROFILE = token => {
  const url = `${GLOBALS.BASE_URL}user/me/profile`
  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  })
  .then(response => response.json())
}

export const UPDATE_USER_PROFILE = (token, newProfile) => {
  const url = `${GLOBALS.BASE_URL}user/me/update`
  return fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify(newProfile)
  })
  .then(response => response.json())
}

/**
 * Change password
 * @param {string} token
 * @param {string} password
 * @param {string} cpassword
 */
export const PASSWORD_RESET = (token, password, cpassword) => {
  const url = `${GLOBALS.BASE_URL}auth/user/password_reset/change_password`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify({
      new_password: password,
      new_cpassword: cpassword
    })
  })
  .then(response => response.json())
}

/**
 * Get product
 * @param {string} token
 * @param {string} id
 */
export const GET_PRODUCT = (token, id = '') => {
  const url = `${GLOBALS.BASE_URL}product/${id}`
  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  })
  .then(response => response.json())
}

/**
 * For sending a forgot password link
 * @param {string} emailPhone
 */
export const FORGOT_PASSWORD = (emailPhone) => {
  const url = `${GLOBALS.BASE_URL}auth/user/password_reset/change_password`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ emailPhone })
  })
  .then(response => response.json())
}

/**
 * Get investment approach for all or specified user
 * @param {string} token
 * @param {string} id
 */
export const GET_INVESTMENT_APPROACH = (token, id = '') => {
  const url = `${GLOBALS.BASE_URL}investment_approach/${id}`
  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  })
  .then(response => response.json())
}

/**
 * reset a user's password of the user
 * @param {string} token
 * @param {string} oldPassword
 * @param {string} newPassword
 */
export const RESET_PASSWORD = (token, oldPassword, newPassword) => {
  const url = `${GLOBALS.BASE_URL}auth/user/password_reset/change_password`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify({
      old_password: oldPassword,
      new_password: newPassword,
      new_cpassword: newPassword
    })
  })
  .then(response => response.json())
}

/**
 * Update user's password
 * @param {*} token
 * @param {*} oldPassword
 * @param {*} newPassword
 */
export const UPDATE_PASSWORD = (token, oldPassword, newPassword) => {
  const url = `${GLOBALS.BASE_URL}user/me/change_password`
  return fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify({
      old_password: oldPassword,
      new_password: newPassword,
      new_cpassword: newPassword
    })
  })
  .then(response => response.json())
}

/**
 * Get all banks
 */
export const GET_ALL_BANKS = () => {
  const url = `${GLOBALS.BASE_URL}helpers/banks`
  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(response => response.json())
}

/**
 * Get current logged in user's bank
 * @param {string} token
 */
export const ADD_BANK = (token, bankInfo) => {
  const url = `${GLOBALS.BASE_URL}user/me/bank`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify(bankInfo)
  })
  .then(response => response.json())
}

/**
 * this endpoint should be called only when the user is selecting the product for the first time
 * this endpoint will return a url that the user has to go to pay, until payment is confirmed the selection is not complete
 * based on the parameters sent, this endpoint will know if to create a recurring payment sample response
 * @param {string} token
 * @param {string} id
 * @param {object} options
 */
export const PRODUCT_SELECTION = (token, id, options) => {
  const url = `${GLOBALS.BASE_URL}product/${id}/selection`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify(options)
  })
  .then(response => response.json())
}

/**
 * verify product payment transaction
 * @param {string} token
 * @param {string} id
 * @param {string} reference
 */
export const PRODUCT_VERIFY_TRANSACTION = (token, id, reference) => {
  const url = `${GLOBALS.BASE_URL}product/transaction/verify`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify({ reference })
  })
  .then(response => response.json())
}

/**
 * This endpoint validate user confirmation code sent during password reset process
 * @param {string} token
 * @param {string} code
 */
export const PASSWORD_RESET_VALIDATE = (token, code) => {
  const url = `${GLOBALS.BASE_URL}auth/user/password_reset/validate`
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify({ code })
  })
  .then(response => response.json())
}

export const GET_DASHBOARD = token => {
  const url = `${GLOBALS.BASE_URL}user/me/dashboard`
  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  })
  .then(response => response.json())
}

export const GET_EARNINGS = token => {
  const url = `${GLOBALS.BASE_URL}user/me/earning`
  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  })
  .then(response => response.json())
}

export const GET_OVERALL_EARNINGS = token => {
  const url = `${GLOBALS.BASE_URL}user/earning/overall`
  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  })
  .then(response => response.json())
}
