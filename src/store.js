import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'remote-redux-devtools';

import rootReducer from './reducers';

const compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || composeWithDevTools
export const store = createStore(
  rootReducer,
  {},
  compose(
    applyMiddleware(
      thunkMiddleware
    )
  )
);
