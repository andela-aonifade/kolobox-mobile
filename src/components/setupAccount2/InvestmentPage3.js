import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { AccountHeader } from '../common';
import AccountDetailPage from './SetupAccount2AccountDetail';

class InvestmentPage3 extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AccountHeader showSkip headerTxt={'Kolobox'} />
        <View style={Styles.imageView} >
          <Text style={Styles.textStyles} >Please confirm investment and connect bank</Text>
        </View>
        <AccountDetailPage product={this.props.product} />
      </View>
    );
  }
}

const Styles = {
  HeaderImage: {
    height: 25,
    width: 25,
    padding: 10
  },
  imageView: {
    justifyContent: 'center',
    height: 40,
    flexDirection: 'row',
    margin: 5
  },
  textStyles: {
    fontSize: 14,
    color: '#000000',
    padding: 5,
  }
};

export default InvestmentPage3;
