import React, { Component } from 'react';
import { View, WebView, Dimensions, Alert, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { AccountHeader } from '../common';
import GLOBALS from '../../globals';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

class Webview extends Component {

  componentWillUnmount() {
    AsyncStorage.getItem('userData', (err, result) => {
      this.data = JSON.parse(result);
      // this.setState({ verifying: true })
      fetch(`${GLOBALS.BASE_URL} product/transaction/verify`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.data.token}`
        },
        body: JSON.stringify({
          reference: this.props.reference
        })
      }).then((response) => response.json())
        .then(responseJson => {
          if (responseJson.status !== true && responseJson.message.name === 'TokenExpiredError') {
            AsyncStorage.removeItem('userData', (error) => {
              if (error !== null) {
                console.log('unable to log out');
                console.log(err);
              }
              Actions.loginPages();
            });
          } else {
            // //this.setState({ verifying: false })
            if (responseJson.status === true) {
              Alert.alert(
                'Success',
                responseJson.message,
                [
                  { text: 'OK' }
                ],
                { cancelable: false }
              );
            } else {
              // //this.setState({ verifying: false })
              Alert.alert(
                'Error!',
                responseJson.message,
                [
                  { text: 'OK' }
                ],
                { cancelable: false }
              );
            }
          }
        })
        .catch((error) => {
          // this.setState({ verifying: false })
          console.error(error);
        });
    });
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <AccountHeader headerTxt={'Kolobox'} />
        <WebView source={{ uri: this.props.authorization_url }} style={[Styles.webview]} />
      </View>
    );
  }
}

const Styles = {
  webview: {
    width: deviceWidth,
    height: deviceHeight
  }
};
export default Webview;
