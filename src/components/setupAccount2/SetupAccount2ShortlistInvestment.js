import React, { Component } from 'react';
import { ScrollView, View, Text, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import SetupAccountProductDetail from './SetupAccountShortlist';
// import { FullWidthButton } from '../common'
import { Spinner } from '../common/spinner';
import { setShortlisted } from '../../actions/investment-actions';

class SetupAccount2ShortlistInvestment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  handleProceed() {
    const { shortlisted, setShortlisted } = this.props;
    this.setState({
      loading: true
    });
    setShortlisted(shortlisted)
    .then(() => Actions.InvestmentPage3())
    .catch(error => {
      Alert.alert('Error', error.message, [{ text: 'Close',
        onPress: () => this.setState({ loading: false }) }]);
    });
  }

  renderProducts() {
    const { shortlisted } = this.props;
    const productView = [];
    const products = [];
    Object.keys(shortlisted).forEach(key => products.push(shortlisted[key]));
    products
      .forEach((product, index) => 
        productView.push(
          <SetupAccountProductDetail 
            key={index} 
            product={product} 
            hidecheckbox handleCheckToggle={() => {}} 
          />
        ));

    return productView.length 
    ? productView 
    : <View 
        style={{ 
          marginTop: 50, 
          flex: 1, 
          flexDirection: 'column', 
          justifyContent: 'center', 
          alignItems: 'center' 
        }}
    > <Text>No product selected</Text>
    </View>;
  }

  render() {
    return (
      <View style={{ flex: 1 }} >
        <ScrollView style={{ flex: 1 }} >
          {this.state.loading ? <Spinner /> : this.renderProducts() }
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (
  { user: { userData: { token } }, investment: { shortlisted } }) => ({ token, shortlisted }
);

const mapDispatchToProps = ({
  setShortlisted
});

export default connect(mapStateToProps, mapDispatchToProps)(SetupAccount2ShortlistInvestment);
