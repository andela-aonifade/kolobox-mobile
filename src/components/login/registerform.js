// {This is a Registration page to create user Account.}
import React, { Component } from 'react';
import { View, ScrollView, Image, Text, TouchableOpacity, Platform, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { Input, FullWidthButton, Spinner } from '../common';
import DatePicker from '../common/datePicker';
import { signUp } from '../../actions/user-actions';

export class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      email: '',
      password: '',
      dob: '',
      phone: '',
      occupation: '',
      loading: false
    };
  }

  handleToUpdate(val) {
    this.setState({ dob: val });
  }

  // {This is a function to create or signup user account.}
  routeTo() {
    if (this.state.firstname === '' || this.state.lastname === '' ||
      this.state.phone === '' || this.state.email === '' ||
      this.state.password === '' || this.state.dob === '' ||
      this.state.occupation === '') {
      Alert.alert(
        'Error!',
        'Please Fill all the fields',
        [
          { text: 'OK', onPress: () => { } }
        ],
        { cancelable: false }
      );
      return;
    }

    this.setState({ loading: true });

    const { email, firstname, lastname, dob, occupation, phone, password } = this.state;
    const cpassword = password;
    const body = { email, firstname, lastname, dob, occupation, phone, password, cpassword };

    const { signUp } = this.props;

    return signUp(body)
    .then(({ status, message, data }) => {
      this.setState({ loading: false });
      if (!status) {
        Alert.alert(
          'Error',
          message,
          [
            { text: 'OK', onPress: () => this.setState({ loading: false }) }
          ],
          { cancelable: false }
        );
        return;
      }

      Alert.alert(
        'Success!',
        `Thank you for signing up with Kolobox.
        Please check your email for activation pin to proceed.`,
        [
          { text: 'OK', onPress: () => Actions.verifyPin() }
        ],
        { cancelable: false }
      );
    })
    .catch(error => {
      Alert.alert(
        'Error',
        error.message,
        [
          { text: 'OK', onPress: () => this.setState({ loading: false }) }
        ],
        { cancelable: false }
      );
      throw error;
    });
  }

  renderButton() {
    if (!this.state.loading) {
      return (
        <FullWidthButton onPress={this.routeTo.bind(this)} Title={'Submit'} />
      );
    }
    return (<Spinner />);
  }

  render() {
    return (
      <ScrollView style={Styles.cover}>
        <View style={{ flex: 1 }}>
          <Input
            label={'user'}
            onChangeText={firstname =>
              this.setState({ firstname })
            }
            placeholder={'First Name'}
            keyboardType={'default'}
          />
          <Input
            label={'user'}
            onChangeText={lastname =>
              this.setState({ lastname })
            }
            placeholder={'Last Name'}
            keyboardType={'default'}
          />
          <Input
            label={'envelope'}
            onChangeText={email =>
              this.setState({ email })
            }
            placeholder={'Email'}
            keyboardType={'email-address'}
          />
          <Input
            label={'user-md'}
            onChangeText={occupation =>
              this.setState({ occupation })
            }
            placeholder={'Occupation'}
            keyboardType={'default'}
          />
          <Input
            label={'lock'}
            onChangeText={password =>
              this.setState({ password })
            }
            placeholder={'Password'}
            secureTextEntry
            keyboardType={'default'}
          />
          <DatePicker
            label={'calendar'}
            handleToUpdate={this.handleToUpdate.bind(this)}
            placeholder={'Date of Birth'}
          />
          <Input
            label={'phone'}
            onChangeText={phone =>
              this.setState({ phone })
            }
            placeholder={'Mobile Number'}
            keyboardType={'phone-pad'}
          />
          <View style={Styles.iacceptContainerStyle}>
            <Image style={Styles.checkBoxImage} source={require('../img/check.png')} />
            <Text style={Styles.textStyles} >
              I accept the </Text>
            <TouchableOpacity
              onPress={Actions.Terms}
              style={{ justifyContent: 'center', padding: 0 }}
            >
              <Text style={Styles.termsAndConditionTextStyle} >terms and conditions</Text>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 15, marginBottom: 15 }}>
            {this.renderButton()}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const Styles = {
  cover: {
    flex: 1,
    paddingLeft: 8,
    paddingRight: 8
  },
  iacceptContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#ffffff',

    margin: 10
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    margin: 5,
    padding: 5,
    justifyContent: 'flex-start'
  },
  textStyles: {
    fontSize: 16,
    color: '#000000',
    alignSelf: 'center',
    marginTop: Platform.OS === 'ios' ? 10 : 0
  },
  termsAndConditionTextStyle: {
    fontSize: 16,
    color: '#3d93cb',
    alignSelf: 'center',
    textDecorationLine: 'underline',
    marginTop: Platform.OS === 'ios' ? 10 : 0
  }
};

const mapDispatchToProps = ({
  signUp
});

export default connect(null, mapDispatchToProps)(RegisterForm);
