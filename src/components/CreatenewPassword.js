// This file used for confirm password
import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, AsyncStorage, Alert } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Spinner } from './common'

class CreatenewPassword extends Component {
  constructor () {
    super()
    this.data = null
    this.state = {
      cpas: '',
      newpas: '',
      updating: false
    }
  }
  // API call for update password
  updatePasswordApi () {
    AsyncStorage.getItem('userData', (err, result) => {
      this.data = JSON.parse(result)
      this.setState({ updating: true })
      fetch(GLOBALS.BASE_URL + 'auth/user/password_reset/change_password', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.data.token
        },
        body: JSON.stringify({
          new_password: this.state.newpas,
          new_cpassword: this.state.cpas
        })
      }).then((response) => response.json())
        .then(responseJson => {
          if (responseJson.status !== true && responseJson.message.name === 'TokenExpiredError') {
            AsyncStorage.removeItem('userData', (error) => {
              if (error !== null) {
                console.log('unable to log out')
                console.log(err)
              }
              Actions.loginPages()
            })
          } else {
            this.setState({ updating: false })
            if (responseJson.status === true) {
              Alert.alert(
                'Success',
                responseJson.message,
                [
                  { text: 'OK', onPress: () => Actions.mainPages() }
                ],
                { cancelable: false }
              )
            } else {
              Alert.alert(
                'Error!',
                responseJson.message,
                [
                  { text: 'OK' }
                ],
                { cancelable: false }
              )
            }
          }
        })
        .catch((error) => {
          console.error(error)
        })
    })
  }
  // this method used for render update button
  renderButton () {
    if (!this.state.updating) {
      return (
        <TouchableOpacity onPress={this.updatePasswordApi.bind(this)}>
          <Text style={Styles.ForgotText}>
            Update
          </Text>
        </TouchableOpacity>
      )
    } else {
      return (
        <Spinner />
      )
    }
  }
  // this method used for render complete UI.
  render () {
    const { cover } = Styles
    return (
      <View style={cover}>
        <View style={{ flex: 1, flexDirection: 'column', paddingTop: 100 }}>
          <View style={Styles.benefitsContainerStyle}>
            <Image style={{ padding: 0 }} source={require('./img/old_pass.png')} />
            <TextInput
              onChangeText={newpas => this.setState({ newpas })}
              underlineColorAndroid='transparent'
              autoCorrect={false}
              secureTextEntry
              placeholder='New Password'
              placeholderTextColor={'#D3D3D3'}
              style={[Styles.InputStyle, { flex: 1 }]} />
          </View>
          <View style={Styles.benefitsContainerStyle}>
            <Image style={{ padding: 0 }} source={require('./img/new_pass.png')} />
            <TextInput
              onChangeText={cpas => this.setState({ cpas })}
              underlineColorAndroid='transparent'
              autoCorrect={false}
              secureTextEntry
              placeholder='Confirm New Password'
              placeholderTextColor={'#D3D3D3'}
              style={[Styles.InputStyle, { flex: 1 }]} />
          </View>
        </View>
        <View style={Styles.forgotButton}>
          {this.renderButton()}
        </View>
      </View>
    )
  }
}
const Styles = {
  forgotButton: {
    flex: 1,
    alignItems: 'center'

  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',
    textDecorationLine: 'underline'
  },
  cover: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 8,
    paddingRight: 8
  },

  HeaderImage: {
    height: 85,
    width: 286,
    alignSelf: 'center'
  },
  InputStyle: {
    flex: 1,
    marginTop: 3,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    paddingBottom: 0

  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 5,
    padding: 0,
    borderBottomWidth: 2,
    borderColor: '#000000'
  }
}
export default CreatenewPassword
