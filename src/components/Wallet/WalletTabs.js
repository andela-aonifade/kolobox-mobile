import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import {
  createNavigator,
  createNavigationContainer,
  TabRouter,
  addNavigationHelpers
} from 'react-navigation';
import WalletWithdrawFund from './WalletWithdrawFund';
import AddFunds from './AddFunds';


const CustomTabBar = ({ navigation }) => {
  const { routes } = navigation.state;
  return (
    <View style={styles.tabContainer}>
      {routes.map(route => (
        <TouchableOpacity
          onPress={() => navigation.navigate(route.routeName)}
          style={styles.tab}
          key={route.routeName}
        >
          <Text>{route.routeName}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const CustomTabView = ({ router, navigation }) => {
  const { routes, index } = navigation.state;
  const ActiveScreen = router.getComponentForState(navigation.state);
  const routeNav = addNavigationHelpers({
    ...navigation,
    state: routes[index],
  });
  const routeOptions = router.getScreenOptions(routeNav, 'tabBar');
console.log(routeOptions.headerTintColor);
  return (
    <View style={styles.container}>
      <CustomTabBar navigation={navigation} />
      <ActiveScreen
        navigation={addNavigationHelpers({
          ...navigation,
          state: routes[index],
        })}
      />
    </View>
  );
};


const CustomTabRouter = TabRouter(
  {
    'Withdraw Funds': {
      screen: WalletWithdrawFund,
      path: '',
    },
    'Add Funds': {
      screen: AddFunds,
      path: '',
    }
  },
  {
    initialRouteName: 'Withdraw Funds',
  },
);

const InvestmentTabs2 = createNavigationContainer(
  createNavigator(CustomTabRouter)(CustomTabView)
);

const styles = StyleSheet.create({
  container: {
    flex: 1
    //marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  tabContainer: {

    flexDirection: 'row',
    height: 48,
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 4,
    borderWidth: 1,
    borderColor: '#ddd',
    backgroundColor: 'white'
  },
});

export default InvestmentTabs2;
