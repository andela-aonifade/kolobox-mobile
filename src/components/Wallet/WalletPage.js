import React, { Component } from 'react';
import { View } from 'react-native';
// import { Actions } from 'react-native-router-flux';
import { AccountHeader } from '../common';
import WalletTabs from './WalletTabs';

class WalletPage extends Component {

  render() {
    return (

      <View style={{ flex: 1 }}>
      <AccountHeader showHelp showWallet headerTxt={'Kolobox'} />
      <WalletTabs />
      </View>
    );
  }
}

export default WalletPage;
