import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  AsyncStorage,
  Alert
} from 'react-native';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
// import Prompt from 'react-native-prompt'
import Prompt from 'react-native-prompt-android';
import { Actions } from 'react-native-router-flux';
import RangeBar from '../setupAccount1/Slider';
import renderIf from '../common/renderIf';
import { FullWidthButton } from '../common';
import BankAccount from '../mainPages/BankAccount';
import GLOBALS from '../../globals';
// import NewBankAccount from '../mainPages/NewBankAccount.js'

class InvestmentPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      status: false,
      bankAccounts: {
        UserBankAccounts: []
      },
      selectedBank: '',
      showPopup: false,
      token: ''
    };
    this.updateSliderValue = this.updateSliderValue.bind(this);
    this.toggleStatus = this.toggleStatus.bind(this);
  }

  componentWillMount() {
    AsyncStorage.getItem('userData', (error, result) => {
      if (result === null) {
        Actions.loginPages();
      } else {
        const data = JSON.parse(result);
        this.setState({
          bankAccounts: data,
          isParsed: true,
          token: data.token
        });
      }
    });
  }

  onSelect(index, value) {
    this.setState({ selectedBank: value });
  }

  showPrompt() {
    if (this.state.selectedBank !== '') {
      Prompt(
        'Please Enter the pin',
        '4 digit pin',
        [
          { text: 'Cancel', onPress: () => this.setState({ showPopup: false }), style: 'cancel' },
          { text: 'OK',
            onPress: (value) => {
              this.setState({ showPopup: false });
              this.withdrawFunds(value);
            }
          }
        ],
        {
          cancelable: false,
          defaultValue: 'test',
          placeholder: 'placeholder'
        }
      );
    } else {
      Alert.alert('Alert!', 'Please select a bank to proceed', 'OK', { cancellable: false });
    }
  }

  withdrawFunds(value) {
    this.setState({ showPopup: false });
    fetch(`${GLOBALS.BASE_URL}/account/transfer`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.state.token}`
      },
      body: JSON.stringify({
        id: this.state.selectedBank,
        pin: value.toString(),
        amount: this.state.value
      })
    }).then((response) => response.json())
      .then(responseJson => {
        if (responseJson.status !== true && responseJson.message.name === 'TokenExpiredError') {
          AsyncStorage.removeItem('userData', (error) => {
            if (error !== null) {
              console.log(error);
            }
            Actions.loginPages();
          })
        } else {
          this.setState({ paying: false })
          if (responseJson.status === true) {
            const data = responseJson.data;
            Alert.alert(
              'Success',
              responseJson.message,
              [
                { text: 'OK' }
              ],
              { cancelable: false }
            );
          } else {
            this.setState({ paying: false })
            Alert.alert(
              'Error!',
              responseJson.message,
              [
                { text: 'OK' }
              ],
              { cancelable: false }
            );
          }
        }
      })
      .catch((error) => {
        this.setState({ paying: false });
        console.error(error);
      });
  }

  toggleStatus() {
    this.setState({
      status: !this.state.status
    });
  }

  updateSliderValue(value) {
    this.setState({ value: parseInt(value, 10) });
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text
          style={{ color: '#2b78e4', fontSize: 18 }}
        >
          Use the web app for withdrawal of fund
        </Text>
      </View>
      // <View style={{ flex: 1, flexDirection: 'column' }}>
      //   <View style={{ flex: 3 }}>
      //     <View style={styles.imageView}>
      //       <Text style={styles.textStyles}>
      //         Please drag to enter amount to withdraw (0 means no money to withdraw)
      //       </Text>
      //     </View>
      //     <View style={styles.benefitsContainerStyle}>
      //       {this.state.isParsed
      //         ? <RangeBar
      //           value={this.state.value}
      //           minval={0}
      //           maxval={parseInt(this.state.bankAccounts.UserWallet.account_balance, 10)}
      //           updateSliderValue={this.updateSliderValue} />
      //         : <Text />
      //       }
      //     </View>
      //     <View style={{ padding: 0, flexDirection: 'row' }}>
      //       <Text style={[styles.lineStyle, { backgroundColor: '#000000', paddingTop: 0 }]} />
      //     </View>
      //   </View>
      //   <View style={{ flex: 10 }}>
      //     <Text style={{ color: '#2b78e4', alignSelf: 'center', fontSize: 15 }}>
      //       NOTE: You cannot withdraw before 30days
      //     </Text>
      //     <TouchableOpacity onPress={() => this.toggleStatus()}>
      //       <View style={[styles.benefitsContainerStyle, { flexWrap: 'wrap', marginTop: 20 }]}>
      //         <Text style={styles.debitCardTextStyle}>
      //           Bank Account
      //         </Text>
      //         <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
      //       </View>
      //     </TouchableOpacity>
      //     <ScrollView>
      //       {renderIf(this.state.status)(
      //         <RadioGroup
      //             onSelect={(index, value) => this.onSelect(index, value)}
      //             selectedIndex={-1}
      //         >
      //           {this.state.bankAccounts.UserBankAccounts.map((prod, index) => {
      //             return (
      //               <RadioButton value={prod.id} key={index}>
      //                 <BankAccount
      //                   bankAccounts={prod}
      //                   key={index}
      //                   handler={this.handler}
      //                   hideRemoveBtn
      //                 />
      //               </RadioButton>
      //             );
      //           }
      //          )}
      //         </RadioGroup>
      //       )}
      //       {renderIf(this.state.status && this.state.bankAccounts.UserBankAccounts < 1)(
      //         <View
      //           style={{
      //             paddingLeft: 10,
      //             paddingRight: 10,
      //             alignItems: 'center',
      //             justifyContent: 'center'
      //           }}
      //         >
      //           <Text style={{ fontSize: 18, color: '#2b78e4' }}>
      //             You have no Bank Account
      //           </Text>
      //           <TouchableOpacity>
      //             <Text style={{ fontStyle: 'italic' }}>Add a Bank</Text>
      //           </TouchableOpacity>
      //         </View>
      //       )}
      //     </ScrollView>
      //   </View>
      //   <View style={{ flex: 1 }}>
      //     <FullWidthButton onPress={this.showPrompt.bind(this)} />
      //   </View>
      // </View>
    );
  }
}

const styles = {
  HeaderImage: {
    height: 25,
    width: 25,
    padding: 10
  },
  imageView: {
    height: 50,
    flexDirection: 'row',
    margin: 5
  },
  textStyles: {
    fontSize: 14,
    color: '#000000',
    padding: 5,
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    marginLeft: 10,
    marginRight: 10,
    padding: 0
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  arrowImageStyle: {
    height: 25,
    width: 25,
    alignSelf: 'flex-end'
  },
  debitCardTextStyle: {
    fontSize: 16,
    color: '#000000',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
}
export default InvestmentPage;
