import React, { Component } from 'react';
import { View, Text } from 'react-native';


export default class TransactionCardDetialRow extends Component {

  render() {
    return (
      <View>
      <Text style={[styles.lineStyle, { backgroundColor: '#000000', marginLeft: 5, marginRight: 5, marginTop: 5, paddingTop: 0 }]} />
      <View style={{ flexDirection: 'row', marginLeft: 5, marginRight: 5, alignItems: 'center' }}>
      <View style={{ flex: 1, alignItems: 'center' }}>
      <Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'12th Jan 17' } </Text>
      </View>
      <View style={{ flex: 1, alignItems: 'center' }}>
      <Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'Investment' } </Text>
      </View>
      <View style={{ flex: 1, alignItems: 'center', borderWidth: 1, borderColor: '#000000', padding: 0 }}>
      <Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'$123' } </Text>
      </View>
      <View style={{ flex: 1, alignItems: 'center', borderWidth: 1, borderColor: '#000000' }}>
      <Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'$123' } </Text>
      </View>
      </View>

      </View>
    );
  }
}
const styles = {
cover: {
  flex: 1,
  flexDirection: 'column',
  paddingLeft: 8,
  paddingRight: 8
},
lineStyle: {
  height: 2,
  backgroundColor: '#2b78e4',
  flex: 1,
  marginTop: 10,
    alignItems: 'center',
      justifyContent: 'center',
},
textStyles: {
 fontSize: 16,
 color: '#ffffff',
 padding: 5,
 flex: 1,
 justifyContent: 'flex-start',
}
};
