//This file used for account details
import React from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native';
import NewBankAccount from './NewBankAccount.js';
import renderIf from '../common/renderIf';
// import DebitCardAdd from '../setupAccount2/debitCardAdd';
// import DebitCardDetails from './DebitcardDetails';
import BankAccount from './BankAccount';

class MyAccountDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: 40,
      status: false,
      status1: false,
      isParsed: false,
      bankAccounts: [],
      updateVal: false
    };
    this.handler = this.handler.bind(this);
  }

  componentWillMount() {
    // AsyncStorage.removeItem('shortlistedProduct');
    AsyncStorage.getItem('userData', (error, result) => {
      console.log(result);
      if (result === null) {
        return;
      }
      this.setState({ bankAccounts: JSON.parse(result), isParsed: true });
    });
  }

handler() {
    //console.log('handler method');
    this.setState({ isParsed: false });
    AsyncStorage.getItem('userData', (error, result) => {
      if (result === null) {
        console.log('null here');
        return;
      }
        // this.bankAccounts = JSON.parse(result);
        this.setState({ bankAccounts: JSON.parse(result), isParsed: true });
        // console.log(result);
  });
  }

//used to set toggle button state
  toggleStatus() {
    this.setState({
      status: !this.state.status,

    });
  }
  toggleStatus1() {
    this.setState({
      status1: !this.state.status1
    });
  }
  toggleStatus2() {
    this.handler();
    this.setState({
      status2: !this.state.status2
    });
  }
  toggleStatus3() {
    this.setState({

        status3: !this.state.status3
    });
  }
  renderProducts() {
       // console.log(this.bankAccounts.UserBankAccounts);
    return (this.state.bankAccounts.UserBankAccounts.map((prod, index) => (
        <BankAccount bankAccounts={prod} key={index} handler={this.handler} />
      ))
    );
  }
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
        <ScrollView >
          <View >
            <TouchableOpacity onPress={this.toggleStatus2.bind(this)}>
              <View style={styles.benefitsContainerStyle}>
                <Text style={styles.debitCardTextStyle} >Bank Account</Text>
                <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
              </View>
            </TouchableOpacity>
            {renderIf(this.state.status2)(
              <ScrollView
                style={{ flex: 1, flexDirection: 'row' }}
                directionalLockEnabled={false}
                horizontal={true}
                pagingEnabled={true}
                scrollEnabled={true}
                decelerationRate={0}
                snapToAlignment='center'
                scrollEventThrottle={16}
              >
                {this.state.isParsed ? this.renderProducts() : <Text /> }
              </ScrollView>
            )}
          </View>
          <View style={{ padding: 0, flexDirection: 'row' }}>
            <Text
              style={[
                styles.lineStyle,
                {
                  backgroundColor: '#000000',
                  marginLeft: 5,
                  marginRight: 5,
                  paddingTop: 0
                }
              ]}
            />
          </View>
          <View>
            <TouchableOpacity onPress={this.toggleStatus3.bind(this)}>
              <View style={styles.benefitsContainerStyle}>
                <Text style={styles.debitCardTextStyle} >New Bank Account</Text>
                <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
              </View>
            </TouchableOpacity>
            {renderIf(this.state.status3)(
              <NewBankAccount />
            )}
          </View>
          <View style={{ padding: 0, flexDirection: 'row' }}>
          <Text
            style={[
              styles.lineStyle,
              {
                backgroundColor: '#000000',
                marginLeft: 5,
                marginRight: 5,
                paddingTop: 0
              }
            ]}
          />
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = {

 blacklineStyle: {
   height: 2,
   backgroundColor: '#2b78e4',
   flex: 1,
   marginTop: 10,
     alignItems: 'center',
       justifyContent: 'center',
 },
 debitCardTextStyle: {
   fontSize: 16,
   color: '#000000',
   flex: 1
 },
 arrowImageStyle: {
   height: 25,
   width: 25,
   padding: 5,
   justifyContent: 'flex-end'
 },
 graphTextStyle: {
   fontSize: 16,
   color: '#000000',
   margin: 10,
   padding: 10
 },
 benefitsContainerStyle: {
  flexDirection: 'row',
  margin: 10,
  padding: 0
},
lineStyle: {
  height: 2,
  backgroundColor: '#2b78e4',
  flex: 1,
  marginTop: 10,
    alignItems: 'center',
      justifyContent: 'center',
},
buttonStyle: {
  height: 30,
  width: 120,
  marginBottom: 5,
  borderRadius: 5,
  borderWidth: 1,
  backgroundColor: '#3d93cb',
  borderColor: '#3d93cb',
},
blackTextStyles: {
 fontSize: 16,
 color: '#3d93cb',
 borderBottomWidth: 2,
 borderColor: '#3d93cb'
}

 }
export default MyAccountDetails;
