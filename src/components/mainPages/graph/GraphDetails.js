import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

class GraphDetails extends Component {
  render() {
    const colors = [
      '#A8C3C8',
      '#C1BADB',
      '#CEA8BC',
      '#A8C3C8',
      '#C1BADB'
    ];
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={{ flex: 1, flexDirection: 'row', margin: 5 }}>
        {
          this.props.investment.map((perInvestment, index) =>
            (<View style={{ flex: 1, flexDirection: 'row' }}>
              <Icon name={'stop'} size={25} color={colors[index]} style={{ padding: 2 }} />
              <Text style={Styles.textStyles}>{ perInvestment }</Text>
            </View>)
          )
        }
        </View>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ padding: 2, paddingRight: 10, fontWeight: 'bold' }}>
              {this.props.labelTotal}
            </Text>
            <Text style={{ padding: 2, paddingRight: 10, fontWeight: 'bold' }}>
              ₦{this.props.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') || '0.000'}
            </Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ padding: 2, fontWeight: 'bold' }}>
              {this.props.labelAverage}
            </Text>
            <Text style={{ padding: 2, fontWeight: 'bold' }}>
              ₦{this.props.average.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') || '0.000'}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const Styles = {

  textStyles: {
    fontSize: 8,
    color: '#000000',
    margin: 10,
    alignSelf: 'center'
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 10
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  benefitTextStyle: {
    fontSize: 16,
    color: '#2b78e4',
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bulletStyle: {
    fontSize: 27,
    color: '#000000'

  },
  bulletTextStyle: {
    fontSize: 16,
    color: '#000000',
    margin: 5,
  },
  buttonStyle: {
    height: 30,
    width: 120,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#3d93cb',
    borderColor: '#3d93cb'
  }
};

export default GraphDetails;
