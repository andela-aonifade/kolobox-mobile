//This file used for inflate Earning Views
import React from 'react';
import { Text, View, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AccountHeader } from '../common';
import EarningsPerformance from './EarningsPerformance';


class Earnings extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Earnings',
    tabBarIcon: () => (<Icon size={24} color="white" name="attach-money" />)
  }

  render() {
    return (
      <View style={{ flex: 1 }} >
        <AccountHeader headerTxt={'Kolonoija'} showHelp />
        <View style={{ flex: 1 }} >
          <ScrollView style={{ flex: 1 }} >
            <Text style={{ alignItems: 'center', justifyContent: 'center', fontSize: 20 }}>
              Welcome to Earnings!
            </Text>
            <EarningsPerformance />
          </ScrollView>
        </View>
      </View>
     );
   }
}

export default Earnings;
