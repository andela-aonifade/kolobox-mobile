//This file used for debit card details
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import SwitchBtn from '../common/switch.js';

export default class DebitCardAdd extends Component {
  constructor(props) {
        super(props);
        this.state = {
           switch1Value: false,
           switch2Value: false,

        };
        console.log(props);
     }
     //used for set toggle button state
     toggleSwitch1 = (value) => {
   this.setState({ switch1Value: value });
}
toggleSwitch2 = (value) => {
this.setState({ switch2Value: value });
}
toggleSwitch3 = (value) => {
this.setState({ switch3Value: value });
}
toggleSwitch4 = (value) => {
this.setState({ switch4Value: value });
}
toggleStatus() {
  this.setState({
    status: !this.state.status
  });
}
  render() {
    const { cover } = Styles;
    return (
<View style={cover}>
<View style={{ flex: 1, flexDirection: 'row' }}>
<View
 style={{ flex: 1, flexDirection: 'column', borderRadius: 5, borderWidth: 1, borderColor: '#000000', margin: 10, paddingLeft: 5, paddingBottom: 5 }}
>
<Icon name={'cc-mastercard'} size={25} color='blue' style={{ padding: 2, height: 25, marginTop: 5, alignSelf: 'flex-end' }} />
<Text style={Styles.ForgotText} >1234 1234 1234</Text>
<Text style={Styles.ForgotText} >01/33</Text>
<Text style={Styles.ForgotText} >John Doe</Text>
</View>

</View>
<View style={{ flex: 1, flexDirection: 'row' }}>
<View style={Styles.benefitsContainerStyle}>
 <Text style={Styles.debitCardTextStyle} >Roundup</Text>
 <View >
         <SwitchBtn
         toggleSwitch1={this.toggleSwitch1}
          toggleSwitch2={this.toggleSwitch2}
          switch1Value={this.state.switch1Value}
          switch2Value={this.state.switch2Value}
         />
         </View>
</View>

</View>
<View style={{ flex: 1, flexDirection: 'row' }}>

<View style={Styles.forgotButton}>
<TouchableOpacity>
<Text style={[Styles.ForgotText, { color: '#2b78e4', textDecorationLine: 'underline', }]} >Remove</Text>
</TouchableOpacity>
</View>

</View>
</View>
    );
  }
}
const Styles = {
cover: {
  flex: 1,
  flexDirection: 'column',
  paddingLeft: 8,
  paddingRight: 8
},
forgotButton: {
flex: 1,
alignItems: 'center',
},
ForgotText: {
fontSize: 16,
color: '#000000',
},
HeaderImage: {
  height: 85,
  width: 286,
  alignSelf: 'center',
},
InputStyle: {
  marginTop: 12,
  paddingRight: 5,
  paddingLeft: 5,
  fontSize: 16,
   paddingBottom: 0,
   height: 25,
   borderBottomWidth: 2,
   borderColor: '#D3D3D3'
},
benefitsContainerStyle: {
 flexDirection: 'row',
 margin: 10,
 padding: 0,
 flex: 1
},
debitCardTextStyle: {
  fontSize: 16,
  color: '#000000',
  flex: 1
},

};
