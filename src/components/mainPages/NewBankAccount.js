// {This file is used to add a new bank account}
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, AsyncStorage, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import ModalSelector from 'react-native-modal-selector';
import { Spinner } from '../common/spinner';

const GLOBALS = require('../../globals');

export default class DebitCardAdd extends Component {
  constructor() {
    super();
    this.data = null;
    this.token = '';
    this.state = {
      switch1Value: false,
      switch2Value: false,
      loading: false,
      textInputValue: '',
      bankCode: '',
      payWithBank: '',
      accontnum: '',
      accountname: '',
      submitaccount: true,
      loadingApi: false,
      data: [{ key: 0, section: true, bankCode: '', label: 'Select Bank' }]

    };
  }
  //This method is used for get list of banks.
  componentWillMount() {
    var index = 1;
    // console.log('bank');
    this.setState({ loading: true });
    // console.log('hellooooo');
    fetch(GLOBALS.BASE_URL + 'helpers/banks', {
      method: 'GET',
      header: {
        'content-type': 'application/json',
      }

    }).then((response) => response.json().then(
      (responseJson) => {
        this.data = response;
        // const responseJson = JSON.parse(responseJs);
        // console.log(response);
        responseJson.data.map(obj => {
          const somearr = this.state.data;
          somearr.push({ key: index++,
            label: obj.name,
            bankCode: obj.code,
            payWithBank: obj.pay_with_bank
          });
          this.setState({ data: somearr });
        });
      })).then(() => {
        this.setState({ loading: false });
      });
  }
  // This method is used to open bank list picker.
  showBanksModal() {
    this.setState({ loading: false });
  }

  toggleSwitch1 = (value) => {
    this.setState({ switch1Value: value });
  }
  toggleSwitch2 = (value) => {
    this.setState({ switch2Value: value });
  }

  loadUserdata() {
    this.setState({ loadingApi: true });
    fetch(GLOBALS.BASE_URL + 'user/me/profile', {
      method: 'GET',
      headers: {
        Authorization: this.token,
        Accept: 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then((response) => response.json()
      .then(responseJson => {
        const token = response.headers.map.token[0];
        this.setState({ loadingApi: false });
        if (responseJson.status !== true && responseJson.message.name === 'TokenExpiredError') {
          AsyncStorage.removeItem('userData', (error) => {
            Actions.loginPages();
          });
        } else {
          if(responseJson.status === true) {
            // console.log(responseJson);
            responseJson.data.token = token;

            AsyncStorage.setItem('userData', JSON.stringify(responseJson.data));
          } else {
            this.setState({ loadingApi: false });
            Alert.alert(
              'Error!',
              responseJson.message,
              [
                { text: 'OK' },
              ],
              { cancelable: false }
            );
          }
        }
      })).catch((error) => {
        this.setState({ loadingApi: false });
        this.setState({ submitaccount: true });
        console.error(error);
      });
  }

  // this method is used to add a new bank account by api.
  addBank() {
    if (this.state.textInputValue === '' || this.state.accountname === ''
      || this.state.accountnum === '') {
      this.setState({ submitaccount: true });
      Alert.alert('Error!',
        'Please Fill all the fields',
        [
          { text: 'OK' },
        ],
        { cancelable: false }
      );
      return;
    }
    this.setState({ loadingApi: true });
    AsyncStorage.getItem('userData', (err, result) => {
      const datan = JSON.parse(result);
      const pay = this.state.payWithBank.toString();
      this.token = `Bearer ${datan.token}`;
      const acc = this.state.accountname;
      const accnum = this.state.accontnum;
      const bankcode = this.state.bankCode;
      const bank = this.state.textInputValue;

      const details = {
        bank_name: bank,
        account_number: accnum,
        account_name: acc,
        bank_code: bankcode,
        pay_with_bank: pay
      };

      let formBody = [];
      details.forEach((property) => {
        const encodedKey = encodeURIComponent(property);
        const encodedValue = encodeURIComponent(details[property]);
        formBody.push(`${encodedKey}=${encodedValue}`);
      });
      formBody = formBody.join('&');

      fetch(GLOBALS.BASE_URL + 'user/me/bank', {
        method: 'POST',
        headers: {
          Authorization: this.token,
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: formBody
      }).then((response) => response.json())
        .then(responseJson => {
          console.log(responseJson);
          if (responseJson.status !== true && responseJson.message.name === 'TokenExpiredError') {
            AsyncStorage.removeItem('userData', () => {
              Actions.loginPages();
            });
          } else {
            if (responseJson.status === true) {
              this.setState({ loadingApi: false });
              this.setState({ submitaccount: false, loading: false });
              Alert.alert(
                'Success',
                'Account added successfully!',
                [
                  { text: 'OK', onPress: () => { this.loadUserdata(); } },
                ],
                { cancelable: false }
              );
            } else {
              this.setState({ loadingApi: false });
              Alert.alert(
                'Error!',
                responseJson.message,
                [
                  { text: 'OK' },
                ],
                { cancelable: false }
              );
            }
          }
        }).catch((error) => {
          this.setState({ loadingApi: false });
          this.setState({ submitaccount: true });
          console.error(error);
        });
    });
  }

  // this method is used to show activity indicator during selection of bank.

  renderText() {
    if (this.state.loading === true || this.state.data.length <= 1) {
      return (<Spinner />);
    }
    return (
      <Text
        style={[
          Styles.InputStyle,
          { flex: 1, marginBottom: 4 },
          this.state.textInputValue && { color: '#000' }]}
      > {this.state.textInputValue || 'Select Bank'}</Text>
    );
  }

  // this method is used to show activity indicator during add account.
  renderAddBankButton() {
    if (this.state.loadingApi === true) {
      return (<Spinner />);
    }
    return (
      <TouchableOpacity onPress={this.addBank.bind(this)} style={{ alignItems: 'center' }}>
        <Text style={[Styles.ForgotText]} >Add Account</Text>
      </TouchableOpacity>
    );
  }

  // this method is used for rendering UI.
  render() {
    const { cover } = Styles;
    return (
      <View style={cover}>

        <View style={{ flex: 1, flexDirection: 'column', paddingBottom: 15 }}>
          <ModalSelector
            data={this.state.data}
            initValue="01"
            supportedOrientations={['portrait']}
            onChange={
              (option) => {
                this.setState({
                  textInputValue: option.label,
                  bankCode: option.bankCode,
                  payWithBank: option.payWithBank
                });
              }}
          >
            <TouchableOpacity style={{ flex: 1 }}>
              <View style={Styles.benefitsContainerStyle}>
                <Image style={{ padding: 0 }} source={require('../img/new_pass.png')} />
                {this.renderText()}
              </View>
            </TouchableOpacity>
          </ModalSelector>
          <View style={Styles.benefitsContainerStyle}>
            <Image style={{ padding: 0 }} source={require('../img/new_pass.png')} />
            <TextInput
              onChangeText={(accontnum) => this.setState({ accontnum })}
              autoCorrect={false}
              placeholder='Account Number'
              placeholderTextColor={'grey'}
              style={[Styles.InputStyle, { flex: 1 }]}
            />
          </View>
          <View style={Styles.benefitsContainerStyle}>
            <Image style={{ padding: 0 }} source={require('../img/new_pass.png')} />
            <TextInput
              onChangeText={(accountname) => this.setState({ accountname })}
              autoCorrect={false}
              placeholder='Account Name'
              placeholderTextColor={'grey'}
              style={[Styles.InputStyle, { flex: 1 }]}
            />
          </View>
          <View style={Styles.benefitsContainerStyle}>
            <Image style={{ padding: 0 }} source={require('../img/new_pass.png')} />
            <TextInput
              autoCorrect={false}
              placeholder='Card No. connected to this account'
              placeholderTextColor={'grey'}
              style={[Styles.InputStyle, { flex: 1 }]}
            />
          </View>
          {this.renderAddBankButton()}
        </View>
      </View>
    );
  }
}
const Styles = {
  cover: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 8,
    paddingRight: 8
  },
  forgotButton: {
    flex: 1,
    alignItems: 'center',
  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',
    textDecorationLine: 'underline',
  },
  HeaderImage: {
    height: 85,
    width: 286,
    alignSelf: 'center',
  },
  InputStyle: {
    flex: 1,
    marginTop: 3,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    paddingBottom: 0,
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 5,
    padding: 0,
    borderBottomWidth: 2,
    borderColor: '#000000'
  }
};
