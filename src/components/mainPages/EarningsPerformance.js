//This file used for Earning performance 
import React, { Component } from 'react';
import { Text, View, ScrollView, TouchableOpacity, Image } from 'react-native';
import Selector from '../common/selector';


export default class EarningsPerformance extends Component {

  render() {
    return (
        <ScrollView style={{ flex: 1 }}>
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>

  <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
<View style={[styles.leftViewStyle, { marginLeft: 5 }]}>
  <Text style={styles.blackTextStyles}>Savings $23456</Text>
  <Text style={styles.blackTextStyles}>Monthly Averages $12345</Text>
</View>
  <Text style={styles.horizontalLineStyle} />
  <View style={[styles.rightViewStyle, { marginLeft: 5 }]}>
    <Text style={styles.blackTextStyles}>Savings $23456</Text>
    <Text style={styles.blackTextStyles}>Monthly Averages $12345</Text>
  </View>

  </View>
  <View style={{ alignItems: 'center', margin: 5 }}>
<Text style={styles.blackTextStyles}>Upto19Aug2017</Text>
<TouchableOpacity style={[styles.buttonStyle, { alignItems: 'center', justifyContent: 'center' }]} >
<Text style={[{ color: '#ffffff' }]}> {'Email Report' } </Text>
</TouchableOpacity>
</View>
<View style={[styles.benefitsContainerStyle, { marginLeft: 10, marginRight: 10, padding: 0 }]}>
<Text style={styles.lineStyle} />
<Text style={styles.benefitTextStyle} >Reports</Text>
  <Text style={styles.lineStyle} />
</View>
<Selector />
<View style={{ flexDirection: 'row', marginLeft: 5, marginRight: 5, marginTop: 5, alignItems: 'center' }}>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'Date' } </Text>
</View>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'Amount' } </Text>
</View>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'Interest' } </Text>
</View>
</View>
<Text style={[styles.lineStyle, { backgroundColor: '#000000', marginLeft: 5, marginRight: 5, marginTop: 0, paddingTop: 0 }]} />
<View style={{ flexDirection: 'row', marginLeft: 5, marginRight: 5, alignItems: 'center' }}>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'12th Jan 17' } </Text>
</View>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'$123456' } </Text>
</View>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'$123' } </Text>
</View>
</View>
<Text style={[styles.lineStyle, { backgroundColor: '#000000', marginLeft: 5, marginRight: 5, marginTop: 0, paddingTop: 0 }]} />
<View style={{ flexDirection: 'row', marginLeft: 5, marginRight: 5, alignItems: 'center' }}>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'12th Jan 17' } </Text>
</View>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'$123456' } </Text>
</View>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'$123' } </Text>
</View>
</View>
<Text style={[styles.lineStyle, { backgroundColor: '#000000', marginLeft: 5, marginRight: 5, marginTop: 0, paddingTop: 0 }]} />
<Text style={[styles.lineStyle, { backgroundColor: '#000000', marginLeft: 5, marginRight: 10, marginTop: 10, paddingTop: 0 }]} />
<View style={styles.benefitsContainerStyle}>
 <Text style={styles.debitCardTextStyle} >Graphic Report</Text>
  <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
</View>
<View
style={{ flexDirection: 'column', flex: 1, borderWidth: 2, borderColor: '#000000', alignItems: 'center' }}
>
  <Text style={styles.graphTextStyle}>Graph</Text>
</View>
</View>
  </ScrollView>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    borderColor: '#4991bf',
    borderWidth: 2,
    margin: 10,
    position: 'relative'
  },
  childContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: '#4991bf',
    padding: 5
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    padding: 5,
    justifyContent: 'flex-end'
  },
   textStyles: {
    fontSize: 16,
    color: '#ffffff',
    padding: 5,
    flex: 1,
justifyContent: 'flex-start',
  },
  blackTextStyles: {
   fontSize: 16,
   color: '#000000',
 },
  interestContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#4991bf',
      padding: 5
  },
  accountContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
      padding: 5
  },
  rightViewStyle: {
        flex: 1,
        alignItems: 'flex-start',
      justifyContent: 'flex-start'
  },
  leftViewStyle: {
      flex: 1,
        alignItems: 'flex-start',
      justifyContent: 'flex-start'
  },
  centerViewStyle: {
      flex: 1,
        alignItems: 'center',
      justifyContent: 'center'
  },
  arrowImageView: {

      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
      flexDirection: 'row'
  },
  horizontalLineStyle: {
    height: 60,
    width: 2,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: 'gray'
  },
  buttonStyle: {
    height: 30,
    width: 120,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#3d93cb',
    borderColor: '#3d93cb',
  },
  benefitsContainerStyle: {
   flexDirection: 'row',
   margin: 10,
   padding: 0
 },
 lineStyle: {
   height: 2,
   backgroundColor: '#2b78e4',
   flex: 1,
   marginTop: 10,
     alignItems: 'center',
       justifyContent: 'center',
 },
 benefitTextStyle: {
   fontSize: 16,
   color: '#2b78e4',
     paddingBottom: 10,
     paddingLeft: 5,
     paddingRight: 5,
   marginBottom: 10,
   alignItems: 'center',
   justifyContent: 'center',
 },
 blacklineStyle: {
   height: 2,
   backgroundColor: '#2b78e4',
   flex: 1,
   marginTop: 10,
     alignItems: 'center',
       justifyContent: 'center',
 },
 debitCardTextStyle: {
   fontSize: 16,
   color: '#000000',
   flex: 1
 },
 arrowImageStyle: {
   height: 25,
   width: 25,
   padding: 5,
   justifyContent: 'flex-end'
 },
 graphTextStyle: {
   fontSize: 16,
   color: '#000000',
   margin: 10,
   padding: 10
 }


};
