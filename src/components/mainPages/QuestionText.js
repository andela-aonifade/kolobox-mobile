import React, { Component } from 'react';
import { Text, View } from 'react-native';


 class QuestionText extends Component {

  render() {
   return (
      <View style={{ flex: 1 }} >
        <View style={styles.childContainerStyle}>
          <Text style={styles.textStyles}>{ this.props.text }
          </Text>
        </View>
      </View>

    );
  }
 }

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    borderColor: '#4991bf',
    borderWidth: 2,
    margin: 10,
    position: 'relative'
  },
  childContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 5
  },

   textStyles: {
    fontSize: 16,
    color: '#000000',
    padding: 5,
    flex: 1,
    justifyContent: 'flex-start',
  },

};

export default QuestionText;
