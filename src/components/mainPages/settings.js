// {This file is used for setting of application}
import React from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Actions } from 'react-native-router-flux'
import { AccountHeader } from '../common'
import SwitchBtn from '../common/switch.js'
import ConfirmPassword from './ChangePassword.js'
import renderIf from '../common/renderIf'


class Settings extends React.Component {

  static navigationOptions = {
    tabBarLabel: 'Setting',
    tabBarIcon: () => (<Icon size={24} color="white" name="settings" />)
  }

  constructor() {
    super()
    this.state = {
      switch1Value: false,
      switch2Value: false,
      switch3Value: false,
      switch4Value: false,
    }
  }
  // This method is used for logout from user account.
  logout() {
    AsyncStorage.removeItem('userData', (err) => {
      AsyncStorage.removeItem('investmentApproach');
      AsyncStorage.removeItem('firstTimeUser');
      AsyncStorage.removeItem('fromforgot');
      
      if (err !== null) {
        console.log('unable to log out')
        console.log(err)
      }
      Actions.loginPages()
    })
  }
  toggleSwitch1 = (value) => {
    this.setState({ switch1Value: value })
  }
  toggleSwitch2 = (value) => {
    this.setState({ switch2Value: value })
  }
  toggleSwitch3 = (value) => {
    this.setState({ switch3Value: value })
  }
  toggleSwitch4 = (value) => {
    this.setState({ switch4Value: value })
  }
  // This is used to change state of toggle button.
  toggleStatus() {
    this.setState({
      status: !this.state.status
    })
  }
  //This method is used for rendring complete UI.
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
        <AccountHeader showHelp showWallet headerTxt={'Kolobox'} />
        <ScrollView >
          <TouchableOpacity onPress={this.toggleStatus.bind(this)}>

            <View style={styles.benefitsContainerStyle}>
              <Text style={styles.debitCardTextStyle} >Change Password</Text>
              <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
            </View>
          </TouchableOpacity>
          {renderIf(this.state.status)(
            <ConfirmPassword />
          )}
          <View style={{ padding: 0, flexDirection: 'row' }}>
            <Text style={[styles.lineStyle, { backgroundColor: '#000000', marginLeft: 5, marginRight: 5, marginTop: 10, paddingTop: 0 }]} />
          </View>
          <View>

            <View style={styles.benefitsContainerStyle}>
              <Text style={styles.debitCardTextStyle} >Touch ID</Text>
              <View >
                <SwitchBtn
                  toggleSwitch1={this.toggleSwitch1}
                  toggleSwitch2={this.toggleSwitch2}
                  switch1Value={this.state.switch1Value}
                  switch2Value={this.state.switch2Value}
                />
              </View>
            </View>
            <View style={{ padding: 0, flexDirection: 'row' }}>
              <Text style={[styles.lineStyle, { backgroundColor: '#000000', marginLeft: 5, marginRight: 5, marginTop: 10, paddingTop: 0 }]} />
            </View>
            <View style={styles.benefitsContainerStyle}>
              <Text style={styles.debitCardTextStyle} >Notification</Text>
              <View >
                <SwitchBtn
                  toggleSwitch1={this.toggleSwitch3}
                  toggleSwitch2={this.toggleSwitch4}
                  switch1Value={this.state.switch3Value}
                  switch2Value={this.state.switch4Value}
                />
              </View>
            </View>
            <View style={{ padding: 0, flexDirection: 'row' }}>
              <Text style={[styles.lineStyle, { backgroundColor: '#000000', marginLeft: 5, marginRight: 5, marginTop: 10, paddingTop: 0 }]} />
            </View>


          </View>

        </ScrollView>

        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-end' }}>
          <TouchableOpacity
            style={[styles.buttonStyle, {
              alignItems: 'center',
              justifyContent: 'center'
            }]}
            onPress={this.logout.bind(this)}
          >
            <Text style={[{ color: '#ffffff' }]}>Logout</Text>
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = {

  blacklineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  debitCardTextStyle: {
    fontSize: 16,
    color: '#000000',
    flex: 1
  },
  arrowImageStyle: {
    height: 25,
    width: 25,
    padding: 5,
    justifyContent: 'flex-end'
  },
  graphTextStyle: {
    fontSize: 16,
    color: '#000000',
    margin: 10,
    padding: 10
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 10,
    padding: 0
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonStyle: {
    height: 30,
    width: 120,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#3d93cb',
    borderColor: '#3d93cb',
  }

}
export default Settings
