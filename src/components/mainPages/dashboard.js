// {this the main InvestmentDetail page. All the screen of investment rendered through it.  }
import React, { Component } from 'react';
import { View } from 'react-native';
// import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AccountHeader } from '../common';
import DashboardTabs from './DashboardTabs';

class Dashboard extends Component {
  static navigationOptions = {
    tabBarLabel: 'Dashboard',
    tabBarIcon: () => (<Icon size={24} color="white" name="home" />)
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AccountHeader showHelp showWallet headerTxt={'Kolobox'} />
        <DashboardTabs />
      </View>
    );
  }
}
// Styles used for View adjustment
const Styles = {
    HeaderImage: {
    height: 25,
    width: 25,
    padding: 10
  },
    imageView: {
    justifyContent: 'center',
    height: 40,
    flexDirection: 'row',
    margin: 5
  },
    textStyles: {
    fontSize: 14,
    color: '#000000',
    padding: 5,
  }
};

export default Dashboard;
