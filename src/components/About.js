import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { AccountHeader } from './common';
import FAQ from './faq';

class About extends Component {
  constructor() {
    super();
    this.state = {
      faqs: [
        {
          title: 'How do I start Investing?',
          text: '\u2022 The process is free, easy and fast \n\u2022 Download the app from Google Play Store or Apple Store, or visit our website to access the web app.\n\u2022Next, enter your details to register. You will receive a validation email immediately. Sign in and choose an investment approach (Cautious, Balanced, Adventurous).\n\u2022Then, select a product and make the initial subscription payment.\n\u2022Select a deduction plan (daily, weekly or monthly) for subsequent deposits and connect your bank card/bank account.\n\u2022By logging into the app at any time, you can view your investment portfolio and interest; add funds to your selected product using the quick save button; withdraw funds from your wallet; and make changes to your account.\n\u2022You can cancel your subscription at any time.\nPayments will automatically be routed from your atm card/ bank account to custodian.'
        },
        {
          title: 'Are there bank charges?',
          text: 'No it is Completely Free! There are no bank charges for transferring your account and withdrawals'
        },
        {
          title: 'How Much Can I start with?',
          text: 'No Mimimum required- you can start with any amount'
        },
        {
          title: 'Are your funds safe?',
          text: 'All financial information is encrypted and stored by our banking partner to PCI DSS Level 1 compliant standards.'
        },
        {
          title: 'Are Deposits Insured?',
          text: 'Savings and Fixed Deposit products held by our partner bank, who are insured by the NDIC - Nigeria Deposit Insurance Corporation, is an independent agency of the Federal Government of Nigeria.'
        },
        {
          title: 'Are my card details safe?',
          text: 'Your security is our only priority. Your card details are extremely safe as they are never stored by Kolobox. We work with a PCIDSS-compliant payment processor to handle your card details'
        },
        {
          title: 'How does Investment work?',
          text: 'Your security is our only priority. Your card details are extremely safe as they are never stored by Kolobox. We work with a PCIDSS-compliant payment processor to handle your card details'
        },
        {
          title: 'Can I cancel my Investment?',
          text: 'Yes, you can cancel your investment at anytime. Simply login and cancel your subscriptions on the product page.'
        },
        {
          title: 'What if I don’t have funds in my debit card or bank account?',
          text: 'We will not be able to debit you for that day, week or month. We would only be able to save for you when you have funds in your debit card/bank account'
        },
        {
          title: 'How do I with withdraw from my investment?',
          text: 'You are allowed one free withdrawal every month depending on the product type. The funds will be remitted to your bank account for withdrawals inputed in your dashboard. Click on the "withdraw" option on your dashboard, enter the amount you want to withdraw and your funds will be transferred to your designated bank account immediately.'
        },
        {
          title: 'What if I withdraw over the free withdraws?',
          text: 'Any withdrawal outside the free withdrawal once monthly is subject to a penalty of 10% on returns.'
        },
        {
          title: 'Are there any penalties for withdrawing my funds?',
          text: 'See investment details. Some investment are charged a penalty before the end of the product tenor.Withdrawals made on these investment before the set tenor attracts a penalty of 10% of returns made on the investment.'
        },
        {
          title: 'What happens if I cancel a product before the end of a Tenor?',
          text: 'There are no charges for cancelling an investment before the end of the Tenor but you will lose all of the interest made on this investment.'
        },
        {
          title: 'What is an Investment Approach?',
          text: 'The algorithm will recommend products based on investment approach selected at registration. approach: we have provided 3 simple option based on your risk appetite \nCautious – Aims for interest \nThis is an option aimed at individuals trying to cultivate a savings habit. This are very short term investments with Withdrawals free of charges which are available 30 days after deposits are made and once monthly subsequently \nBalanced – Aim for Interest and Minimal Growth \nThis is an option aimed at individuals trying to save towards a target – a car, rent, school fees etc. This is a medium term invest 90 days circa. The investor selects a Target Withdrawal Date for both the amount invested and the interest. A fixed return is expected on this class of investment. \nAdventurous- Aims for High Growth \nThis is a longer term investments and aimed at individuals who are planning for the future or retirement usually 180 days circa and above (mature in one year or less).'
        } 
      ]
    };
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
        <AccountHeader showWallet headerTxt={'Kolobox'} />
        <ScrollView style={{ marginLeft: 5, marginRight: 5 }}>
          <View style={Styles.benefitsContainerStyle}>
            <Text style={Styles.lineStyle} />
            <Text style={Styles.benefitTextStyle} >About Us?</Text>
            <Text style={Styles.lineStyle} />
          </View>
          <Text style={Styles.textStyles} >
            Kolobox is a platform that works directly with financial institutions
            to enable you to save and invest your spare change.
            The funds in your ewallet account is held by our partner bank,
            We never have custody or access to your funds.
            Your funds are transferred directly from your account to our partner bank.
          </Text>
          <View style={Styles.benefitsContainerStyle}>
            <Text style={Styles.lineStyle} />
            <Text style={Styles.benefitTextStyle} >FAQ</Text>
            <Text style={Styles.lineStyle} />
          </View>
          {
            this.state.faqs.map((faq, index) => 
              <FAQ 
                title={faq.title}
                text={faq.text}
                id={index}
              />
            )
          }
          
        </ScrollView>
      </View>

    );
  }
}

const Styles = {
  textStyles: {
    fontSize: 16,
    color: '#000000',
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 10,
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  benefitTextStyle: {
    fontSize: 16,
    color: '#2b78e4',
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bulletStyle: {
    fontSize: 27,
    color: '#000000',
  },
  bulletTextStyle: {
    fontSize: 16,
    color: '#000000',
    marginLeft: 5,
    marginRight: 5,
  },
  arrowImageStyle: {
    height: 25,
    width: 25,
    padding: 5,
    alignSelf: 'flex-end'
  },
  debitCardTextStyle: {
    fontSize: 16,
    color: '#000000',
    flex: 1
  }
};

export default About;
