import React, { Component } from 'react';
import { Text, View, ScrollView, WebView } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { FullWidthButton } from '../common';

class InvestmentDetailInfo extends Component {
  render() {
    const { product } = this.props;

    return (
      <View style={{ flex: 1 }} >
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
          <ScrollView>
            <Text style={Styles.textStyles} >Terms And Conditions</Text>
            <Text style={Styles.textStyles} >{product.terms_conditions}</Text>
            <View style={Styles.benefitsContainerStyle}>
              <Text style={Styles.lineStyle} />
              <Text style={Styles.benefitTextStyle} >Benefits</Text>
              <Text style={Styles.lineStyle} />
            </View>
            <View style={{ margin: 5 }} >
              <View style={{ flexDirection: 'row' }} >
                <Text style={Styles.bulletStyle}>{'\u2022'}</Text>
                <Text style={Styles.bulletTextStyle}>{product.benefits} </Text>
              </View>

            </View>
            <View style={Styles.benefitsContainerStyle}>
              <Text style={Styles.lineStyle} />
              <Text style={Styles.benefitTextStyle}>Fees</Text>
              <Text style={Styles.lineStyle} />
            </View>
            <View style={{ margin: 5 }} >
              <Text style={Styles.textStyles}>₦ {product.fee || '0.00'}</Text>
            </View>
            <View style={{ margin: 30 }} />

          </ScrollView>
          <FullWidthButton
            Title='I agree to Terms and Conditions'
            onPress={() => {
              Actions.InvestmentPage3({ product });
            }}
          />
        </View>
      </View>

    );
  }
}

const Styles = {
  textStyles: {
    fontSize: 16,
    color: '#000000',
    margin: 10,
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 10
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  benefitTextStyle: {
    fontSize: 16,
    color: '#2b78e4',
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bulletStyle: {
    fontSize: 27,
    color: '#000000'

  },
  bulletTextStyle: {
    fontSize: 16,
    color: '#000000',
    margin: 5,
  }
};

export default InvestmentDetailInfo;
