import React, { Component } from 'react'
import { Text, Image, View, Platform } from 'react-native'

class Header extends Component {
  render () {
    const { HeaderImage, imageView, componentStyle, bigFont, smallFont, mediumFont, HelpImage } = Styles
    return (

      <View style={componentStyle}>
        {/* <View style={{ flex: 1, height: 25, paddingTop: 20, paddingLeft: 5 }}>

              <Image
                style={this.props.showHelp ? HelpImage : { height: 0, width: 0 }}
                source={require('../img/help.png')}
              />

              </View>*/}
        <View style={imageView}>
          <Image style={HeaderImage} source={require('../img/kolo_logo-3.png')} />
        </View>
      </View>

    )
  }
}

/*
const Header = (props) => {
  const { HeaderImage, imageView, componentStyle, bigFont, smallFont, mediumFont } = Styles
  return (

<View style={componentStyle}>
<View style={{ flex: 1, height: 25, paddingTop: 20, paddingLeft: 5 }}>
{

<Image style={HeaderImage} source={require('../img/help.png')} hide={props.showHelp} />

}
</View>
<View style={imageView} >
<Image style={HeaderImage} source={require('../img/hopskotch.png')} />
 <Text style={mediumFont} >{props.headerTxt}</Text>
 </View>

 <View style={imageView} >

  <Text style={bigFont} >Lorem Ipesum Dolem</Text>
  </View>

  <View style={imageView} >

   <Text style={smallFont} >Lorem Ipesum</Text>
   </View>

</View>

  )
}
*/
// Style components

const Styles = {
  HeaderImage: {
    height: 100,
    width: 100
  // borderRadius: 12
  },
  HelpImage: {
    height: 25,
    width: 25
  // borderRadius: 12
  },

  imageView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 2
  },
  mediumFont: {
    paddingLeft: 5,
    fontSize: 20,
    color: '#ffffff'

  },
  bigFont: {
    fontSize: 22,
    color: '#ffffff',

  },
  smallFont: {
    fontSize: 16,
    color: '#ffffff',

  },

  componentStyle: {
    backgroundColor: '#3d93cb',
    justifyContent: 'center',
    flexDirection: 'row',
    position: 'relative',
    padding: 1,
    height: 150
  }
}

// make component available to other parts of App

export { Header }
