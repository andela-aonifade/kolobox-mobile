import React from 'react'
import { View } from 'react-native'

const CardComponent = (props) => (
  <View style={styles.containerStyle}>
    {props.children}
  </View>
)

const styles = {
  containerStyle: {
    backgroundColor: '#2b78e4',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
    padding: 1,
    height: 200
  }
}

export { CardComponent }
