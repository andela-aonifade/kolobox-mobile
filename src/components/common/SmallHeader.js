import React, { Component } from 'react'
import { Button } from 'react-native'

class SmallHeader extends Component {
  render() {
    return (
      <Button
        onPress={this.props.btnpress}
        title='Learn More'
        color='#841584'
        accessibilityLabel='Learn more about this purple button'
      />
    )
  }
}

export default SmallHeader
