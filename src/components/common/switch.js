import React from 'react'
import {
   View,
   Switch,
   StyleSheet
} from 'react-native'

export default SwitchBtn = (props) => (
  <View >

    <Switch
      onValueChange={props.toggleSwitch2}
      value={props.switch2Value} />

  </View>
   )
