import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'

const FullWidthButton = ({ onPress, Title }) => {
  return (
    <View style={{ flex: 1, justifyContent: 'flex-end' }}>
      <TouchableOpacity style={Styles.buttonStyle} onPress={onPress}>
        <Text style={Styles.textStyle}>
          {Title || 'Proceed'}
        </Text>
      </TouchableOpacity>
    </View>
  )
}

const Styles = {
  buttonStyle: {
    height: 40,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#3d93cb',
    borderColor: '#3d93cb'
  },
  textStyle: {
    alignSelf: 'center',
    color: '#ffffff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10
  }
};

export { FullWidthButton };
