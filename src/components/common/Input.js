import React from 'react'
import { View, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const Input = ({ onChangeText, value, placeholder, secureTextEntry, label, keyboardType, size, onBlur }) => (
  <View style={styles.containerStyle}>
    <View style={styles.labelStyle}>
      <Icon
        name={label}
        size={size || 25}
        color='grey'
        style={{ padding: 2 }} />
    </View>
    <TextInput
      underlineColorAndroid='transparent'
      secureTextEntry={secureTextEntry}
      autoCorrect={false}
      placeholder={placeholder}
      placeholderTextColor={'#D3D3D3'}
      style={styles.InputStyle}
      onChangeText={onChangeText}
      keyboardType={keyboardType}
      value={value}
      onBlur={onBlur} 
    />
  </View>
);

const styles = {
  InputStyle: {
    flex: 1,
    marginTop: 0,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    paddingBottom: 5

  },
  labelStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 35
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    height: 35,
    borderBottomWidth: 2,
    marginTop: 15,
    flexWrap: 'wrap',
    borderColor: '#D3D3D3'
  }
}

export { Input }
