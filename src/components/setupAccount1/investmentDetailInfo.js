import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { AccountHeader } from '../common';

class InvestmentDetailInfo extends Component {

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
      <AccountHeader headerTxt="Kolonaija" />
      <ScrollView>
      <Text style={Styles.textStyles} >Terms And Conditions</Text>
      <Text style={Styles.textStyles} >Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Text>
    <View style={Styles.benefitsContainerStyle}>
    <Text style={Styles.lineStyle} />
    <Text style={Styles.benefitTextStyle} >Benefits</Text>
      <Text style={Styles.lineStyle} />
    </View>
    <View style={{ margin: 10 }} >
    <View style={{ flexDirection: 'row' }} >
      <Text style={Styles.bulletStyle}>{'\u2022'}</Text>
      <Text style={Styles.bulletTextStyle}> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </Text>
    </View>
    <View style={{ flexDirection: 'row' }}>
      <Text style={Styles.bulletStyle}>{'\u2022'}</Text>
      <Text style={Styles.bulletTextStyle}>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
    </View>
    <View style={{ flexDirection: 'row'}}>
      <Text style={Styles.bulletStyle}>{'\u2022'}</Text>
      <Text style={Styles.bulletTextStyle}>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</Text>
    </View>
    <View style={{ flexDirection: 'row'}}>
      <Text style={Styles.bulletStyle}>{'\u2022'}</Text>
      <Text style={Styles.bulletTextStyle}>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Text>
    </View>
    </View>
    <View style={Styles.benefitsContainerStyle}>
    <Text style={Styles.lineStyle} />
    <Text style={Styles.benefitTextStyle} >Fees</Text>
      <Text style={Styles.lineStyle} />
    </View>
    <Text style={Styles.textStyles}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Text>
        </ScrollView>
      </View>

    );
  }

}

const Styles = {

  textStyles: {
    fontSize: 16,
    color: '#000000',
    margin: 10,
},
 benefitsContainerStyle: {
  flexDirection: 'row',
  margin: 10
},
lineStyle: {
  height: 2,
  backgroundColor: '#2b78e4',
  flex: 1,
    alignItems: 'center'
},
benefitTextStyle: {
  flex: 1,
  fontSize: 16,
  color: '#2b78e4',
  padding: 5,
  alignItems: 'center',
  justifyContent: 'center',
},
bulletStyle: {
  fontSize: 27,
  color: '#000000',

},
bulletTextStyle: {
  fontSize: 16,
  color: '#000000',
  margin: 5,
}
};

export default InvestmentDetailInfo;
