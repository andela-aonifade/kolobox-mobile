import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import RangeBar from './Slider';
import { AccountHeader } from '../common';

class InvestmentDetailEligibility extends Component {
  constructor(props) {
     super(props);
     this.state = { value: 40,
value1: 36
    };
}
onSelect(index, value) {
  this.setState({
  text: value
  });
}
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
      <AccountHeader headerTxt="Kolonaija" />
      <ScrollView>
      <Text style={Styles.textStyles} >Amount(in ₦)</Text>
      <View style={Styles.benefitsContainerStyle}>
       <RangeBar
       value={this.state.value}
       maxval={100}
       onValueChange={value => this.setState({ value })}
       />
         <View style={Styles.selectedPriceTextStyle}>
        <Text>{this.state.value}</Text>
        </View>
         </View>
         <View style={Styles.pickerContainerStyle}>
           <Text style={Styles.textStyles} >Per</Text>
           <View style={Styles.selectedPriceTextStyle}>
          <Text style={Styles.textStyles} >Month</Text>
          </View>
         </View>
         <Text style={Styles.textStyles} >Investment Approach</Text>
         <View style={Styles.containerRAdio}>

             <RadioGroup
             style={Styles.containerRAdio}
               onSelect={(index, value) => this.onSelect(index, value)}
               selectedIndex={0}
             >
               <RadioButton value={'Cautions'} >
                 <Text>Cautions</Text>
               </RadioButton>

               <RadioButton value={'Balanced'}>
                 <Text>Balanced</Text>
               </RadioButton>

               <RadioButton value={'Adventurous'}>
                 <Text>Adventurous</Text>
               </RadioButton>
             </RadioGroup>

           </View>
           <Text style={Styles.textStyles} >Tenure(in Months)</Text>
           <View style={Styles.benefitsContainerStyle}>
            <RangeBar
            style={{ padding: 0 }}
            value1={this.state.value1}
            maxval={50}
            onValueChange={value1 => this.setState({ value1 })}
            />
              <View style={Styles.selectedPriceTextStyle}>
             <Text>{this.state.value1}</Text>
             </View>
              </View>
              <View style={Styles.benefitsContainerStyle}>
              <Text style={Styles.lineStyle} />
              <Text style={Styles.benefitTextStyle} >Projections</Text>
                <Text style={Styles.lineStyle} />
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  flex: 1,
                  borderWidth: 2,
                  borderColor: '#000000',
                  alignItems: 'center'
                }}
              >
                <Text style={Styles.graphTextStyle}>Graph</Text>
              </View>
        </ScrollView>
      </View>

    );
  }

}

const Styles = {

  textStyles: {
    fontSize: 16,
    color: '#000000',
    margin: 10,
},
 benefitsContainerStyle: {
  flexDirection: 'row',
  margin: 10,
  padding: 0
},
lineStyle: {
  height: 2,
  backgroundColor: '#2b78e4',
  flex: 1,
    alignItems: 'baseline',
      justifyContent: 'center',
},
benefitTextStyle: {
  fontSize: 16,
  color: '#2b78e4',
  padding: 5,
  marginBottom: 10,
  alignItems: 'flex-start',
  justifyContent: 'center',
},
bulletStyle: {
  fontSize: 27,
  color: '#000000',

},
bulletTextStyle: {
  fontSize: 16,
  color: '#000000',
  margin: 5,
},
container: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  selectedPriceTextStyle: {
  borderBottomColor: '#000000',
  borderBottomWidth: 1,
  width: 70,
  alignItems: 'center'
},
pickerContainerStyle: {
  flexDirection: 'row',
  margin: 10,
  justifyContent: 'center',
    alignItems: 'center'
},
containerRAdio: {
       flexDirection: 'row',
   },
   text: {
       padding: 10,
       fontSize: 14,
   },
   graphTextStyle: {
     fontSize: 16,
     color: '#000000',
     margin: 10,
     padding: 10
   }

};

export default InvestmentDetailEligibility;
