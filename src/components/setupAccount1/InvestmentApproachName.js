import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';


 class InvestmentApproachName extends Component {
   constructor(props) {
    super(props);
    this.state = {
      text: ''
    }
  }
   onSelect(index, value) {
     this.setState({
       text: value
     });
   }

   getRadio(invest) {
     return (
       <RadioButton value={invest.id} key={invest.id}>
         <Text>{invest.name}</Text>
       </RadioButton>
     );
   }

   renderRadio() {
     return (this.props.investment.map(invest => this.getRadio(invest)));
   }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }} key={this.props.investment.id}>
        <Text style={Styles.textStyles} >Select an Investment approach</Text>
        <View style={Styles.containerRAdio}>
          <RadioGroup
            style={Styles.containerRAdio}
            onSelect={(index, value) => this.onSelect(index, value)}
            selectedIndex={0}
          >
            {this.renderRadio()}
          </RadioGroup>
        </View>
      </View>
    );
  }
 }

const Styles = {
  textStyles: {
    fontSize: 14,
    color: '#000000',
    padding: 5,
    marginTop: 5
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    padding: 5,
    marginTop: 5
  },
  containerRAdio: {
    flexDirection: 'column',
  }
};

export default InvestmentApproachName;
