import React from 'react';
import { Text, View } from 'react-native';
import { TabNavigator } from 'react-navigation';
import { SetupAccountHeader } from '../common';

class RecentChatsScreen extends React.Component {
  render() {
    return <Text>List of recent chats</Text>;
  }
}

class AllContactsScreen extends React.Component {
  render() {
    return <Text>List of all contacts</Text>;
  }
}

const InvestmentInfoTab = TabNavigator({
  Recent: { screen: RecentChatsScreen },
  All: { screen: AllContactsScreen }
});

export default InvestmentInfoTab;
