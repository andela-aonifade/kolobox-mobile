import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import { AccountHeader } from '../common';

class InvestmentDetail extends Component {

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
      <AccountHeader headerTxt="Kolonaija" />
      <View style={Styles.childContainerStyle}>
          <Text style={Styles.textStyles}>Name of the Deposit</Text>
          <Image style={Styles.checkBoxImage} source={require('../img/check.png')} />
      </View>
      </View>

    );
  }

}

const Styles = {

  childContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: '#ffffff',
    padding: 5
  }
};

export default InvestmentDetail;
