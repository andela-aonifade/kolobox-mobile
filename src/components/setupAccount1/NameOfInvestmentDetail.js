import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
// import { Actions } from 'react-native-router-flux';
import CheckBox from 'react-native-check-box';

class NameOfInvestmentDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: !!props.checked
    };
  }

  componentWillMount() {

  }

  onClick() {
    const { handleCheckToggle, category } = this.props;
    const { checked } = this.state;
    const newCheckedState = !checked;

    handleCheckToggle(category, newCheckedState);
    this.setState({
      checked: newCheckedState
    });
  }
  render() {
    // const routeTo = () => {
    //   const category = this.props.category;
    //   Actions.InvestmentDetailsPage({ category });
    // };

    return (
      <View style={styles.containerStyle}>
        <View style={styles.childContainerStyle}>
          <TouchableOpacity
            onPress={this.props.modalVisible(true)}
            style={{ flex: 1, justifyContent: 'flex-start' }}
          >
            <Text style={styles.textStyles}>{this.props.category.name}</Text>
          </TouchableOpacity>
          <View style={(this.props.hidecheckbox) ? { display: 'none' } : ''}>
            <CheckBox
              isChecked={this.state.checked}
              onClick={this.onClick.bind(this)}
              unCheckedImage={
                <Image
                  style={styles.checkBoxImage}
                  source={require('../img/uncheck.png')}
                />
              }
              checkedImage={
                <Image
                  style={styles.checkBoxImage}
                  source={require('../img/check_white.png')}
                />
              }
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    borderColor: '#4991bf',
    borderWidth: 2,
    margin: 10,
    position: 'relative'
  },
  childContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: '#4991bf',
    padding: 5
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    padding: 5,
    marginTop: 5
  },
  textStyles: {
    fontSize: 16,
    color: '#ffffff',
    padding: 5,
    flex: 1,
    justifyContent: 'flex-start',
  },
  blackTextStyles: {
    fontSize: 16,
    color: '#000000',
    padding: 5,
  },
  interestContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#4991bf',
    padding: 5
  },
  accountContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 5
  },
  rightViewStyle: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  leftViewStyle: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  arrowImageView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row'
  }
};


export default NameOfInvestmentDetail;
