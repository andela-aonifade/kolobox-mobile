import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import renderIf from './common/renderIf';
import QuestionText from './mainPages/QuestionText.js';

class FAQ extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      status: false
    };
  }

  toggleStatus() {
    this.setState({
      status: !this.state.status
    });
  }

  render() {
    return (
      <View>
        <TouchableOpacity onPress={this.toggleStatus.bind(this)} style={{ marginTop: 5 }}>
          <View style={Styles.benefitsContainerStyle}>
            <Text style={Styles.debitCardTextStyle} >{this.props.title}</Text>
            <Image style={Styles.arrowImageStyle} source={require('./img/down_arrow.png')} />
          </View>
        </TouchableOpacity>
        {renderIf(this.state.status)(
          <QuestionText 
            text={this.props.text}
          />
        )}
        <View style={{ padding: 0, flexDirection: 'row' }}>
          <Text style={[Styles.lineStyle, { backgroundColor: '#000000', paddingTop: 0 }]} />
        </View>
      </View>
    );
  }
}

const Styles = {
  textStyles: {
    fontSize: 16,
    color: '#000000',
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 10,
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  benefitTextStyle: {
    fontSize: 16,
    color: '#2b78e4',
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bulletStyle: {
    fontSize: 27,
    color: '#000000',
  },
  bulletTextStyle: {
    fontSize: 16,
    color: '#000000',
    marginLeft: 5,
    marginRight: 5,
  },
  arrowImageStyle: {
    height: 25,
    width: 25,
    padding: 5,
    alignSelf: 'flex-end'
  },
  debitCardTextStyle: {
    fontSize: 16,
    color: '#000000',
    flex: 1
  }
};


export default FAQ;
