// {This file is used to enter pin to navigate into next screen.}
import React, { Component } from 'react'
import { Text, View, TextInput, Keyboard, Alert } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'

import { AccountHeader, FullWidthButton, Spinner } from './common'
import { setUserPin, getUserProfile } from '../actions/user-actions'

class ChangePincode extends Component {
  constructor (props) {
    super(props)
    this.data = null
    this.state = { text1: '', text2: '', text3: '', text4: '', loading: false }
  }

  // this method contain Api to update user pin.
  updatePin () {
    if (this.state.text1 === '' || this.state.text2 === '' ||
      this.state.text3 === '' || this.state.text4 === '') {
      Alert.alert(
        'Error!',
        'Please Fill all the fields',
        [
          { text: 'OK' }
        ],
        { cancelable: false }
      )
      return
    }
    const { text1, text2, text3, text4 } = this.state
    const { setUserPin, getUserProfile, token } = this.props
    const pin = `${text1}${text2}${text3}${text4}`
    this.setState({loading: true})

    setUserPin(token, pin)
    .then(responseData => {
      const {status, message} = responseData
      this.setState({loading: false})
      if (!status) {
        Alert.alert(
          'Error',
           message,
          [
            { text: 'OK' }
          ],
          { cancelable: false }
        )
        throw new Error(message)
      }

      return getUserProfile(token, true)
    })
    .then(() => {
      Alert.alert(
        'Success',
        'Pin has been set successfully!!',
        [
          { text: 'OK', onPress: () => Actions.InvestmentPages() }
        ],
        { cancelable: false }
      )
    })
    .catch(error => {
      this.setState({loading: false})
      Alert.alert(
        'Error',
       error.message,
        [
            { text: 'OK' }
        ],
        { cancelable: false }
      )
      throw error
    })
  }
  renderButton () {
    if (!this.state.loading) {
      return (
        <FullWidthButton onPress={this.updatePin.bind(this)} Title={'Create Pin'} />
      )
    } else {
      return (
        <Spinner />
      )
    }
  }
  // This render method used for rendering UI.
  render () {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
        <AccountHeader headerTxt='Kolobox' showHelp />
        <View style={Styles.componentStyle}>
          <Text style={Styles.textStyles}>
            Create a pin
          </Text>
          <View style={Styles.textInputContainerStyle}>
            <TextInput
              autoFocus
              style={Styles.textInputStyle}
              onChangeText={
                text1 => {
                  this.setState({ text1 })
                  if (text1 && text1.length === 1) {
                    this.refs.SecondInput.focus()
                  }
                }
              }
              value={this.state.text1}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={
                event => {
                  this.refs.SecondInput.focus()
                }}
            />
            <TextInput
              ref='SecondInput'
              style={Styles.textInputStyle}
              onChangeText={
                text2 => {
                  this.setState({ text2 })
                  if (text2 && text2.length === 1) {
                    this.refs.ThirdInput.focus()
                  }
                }}
              value={this.state.text2}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={
                event => {
                  this.refs.ThirdInput.focus()
                }}
            />
            <TextInput
              ref='ThirdInput'
              style={Styles.textInputStyle}
              onChangeText={
                text3 => {
                  this.setState({ text3 })
                  if (text3 && text3.length === 1) {
                    this.refs.FourthInput.focus()
                  }
                }}
              value={this.state.text3}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={
                event => {
                  this.refs.FourthInput.focus()
                }} />
            <TextInput
              ref='FourthInput'
              style={Styles.textInputStyle}
              onChangeText={
                text4 => {
                  this.setState({ text4 })
                  if (text4 && text4.length === 1) {
                    Keyboard.dismiss()
                  }
                }}
              value={this.state.text4}
              keyboardType='numeric'
              maxLength={1} />
          </View>
          {this.renderButton()}
        </View>
      </View>
    )
  }
}

// Style components
const Styles = {
  textStyles: {
    fontSize: 20,
    color: '#000000',
    marginTop: 20
  },
  componentStyle: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    flexDirection: 'column',
    padding: 1,
    flex: 1
  },
  textInputContainerStyle: {
    flexDirection: 'row',
    marginTop: 10
  },
  textInputStyle: {
    height: 100,
    width: 80,
    borderColor: 'gray',
    borderWidth: 2,
    margin: 5,
    flex: 2,
    fontSize: 30,
    alignSelf: 'center',
  },
  buttonStyle: {
    height: 40,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#4991bf',
    borderColor: '#4991bf'
  },
  textStyle: {
    alignSelf: 'center',
    color: '#ffffff',
    fontWeight: '600',
    paddingTop: 10
  }
}

const mapStateToProps = ({user: {userData: { token }}}) => ({token})

const mapDispatchToProps = ({
  setUserPin,
  getUserProfile
})

export default connect(mapStateToProps, mapDispatchToProps)(ChangePincode)
