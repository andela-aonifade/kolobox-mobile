// {this the routing file... all routes are defined in this file  }
import React, { Component } from 'react'
import {connect} from 'react-redux'
import {View, TouchableOpacity,Text, AsyncStorage, Image } from 'react-native'
import { Scene, Router, ActionConst, Actions } from 'react-native-router-flux'

import LoginPage from './components/login/loginpage.js'
import ForgotPassword from './components/login/forgotPassword.js'
import TermsAndCondition from './components/login/TermsAndCondition.js'
import InvestmentInfoTab from './components/setupAccount1/InvestmentInfoTabs.js'
import InvestmentDetailEligibility from './components/setupAccount1/InvestmentDetailProjection.js'
import Pincode from './components/pincode.js'
import Validatecode from './components/Validatecode.js'
import CreatenewPassword from './components/CreatenewPassword.js'
import ChangePincode from './components/ChangePincode.js'

import BottomNav from './components/bottomNav'
import InvestmentPage from './components/setupAccount1/InvestmentPage.js'
import AccountDetailPage from './components/setupAccount1/SetupAccount2AccountDetail.js'
import InvestmentDetailsPage from './components/investmentDetail/InvestmentDetailsPage.js'
import InvestmentPage3 from './components/setupAccount2/InvestmentPage3.js'
import Webview from './components/setupAccount2/Webview.js'
import EarningListDetailRow from './components/mainPages/EarningsListRow.js'
import Earnings from './components/mainPages/earnings.js'
import EarningNameOfDeposit from './components/mainPages/EarningNameOfDeposit.js'
import Pager from './components/Pager/Pager.js'
import WalletPage from './components/Wallet/WalletPage.js'
import About from './components/About.js'
import InvestmentApproach from './components/setupAccount1/InvestmentApproach.js'

import ERRORS from './common/errors'
// import { firstTimeUser, getUserData, setFirstTimeUser } from './api/local-data'

import {
  getUserData,
  getFirstTimeUser,
  setFirstTimeUser,
  getUserDashboard
} from './actions/user-actions'


class RouterComponent extends Component {

  constructor () {
    super()
    this.state = {
      loading: true,
      rootPage: ''
    }
  }

  componentWillMount(){
    // AsyncStorage.removeItem('userData');
  }

  componentDidMount () {
    const { getUserData, getFirstTimeUser } = this.props

    if (!this.state.loading) return
    Promise.all([getUserData(), getFirstTimeUser()])
    .then(() => {
      const {userData, firstTimeUser} = this.props
      const newState = {
        loading: false
      }
      // first time
      if (firstTimeUser === true) {
        newState.rootPage = 'Pager'
        this.setState(newState)
        return
      }
      // user has opened app previously, but no user data exist
      if (firstTimeUser === false && !userData.token) {
        newState.rootPage = 'loginPages'
        this.setState(newState)
        return
      }
      // sets initial page based on avilability of user data
      if (userData.token && userData.hasPIN) {
        newState.rootPage = 'mainPages'
        this.postLoginActions(userData.token)
        this.setState(newState)
        return
      }
      // sets initial page based on avilability of user data
      if (userData.token && userData.active && !userData.hasPIN) {
        newState.rootPage = 'changePin'
        this.setState(newState)
        return
      }
      // user has not verified account
      if (userData.token && !userData.active) {
        newState.rootPage = 'verifyPin'
        this.setState(newState)
        return
      }
    })
    .catch(error => {
      this.setState({loading: false})
      throw error
    })
  }

  postLoginActions(token) {
    const { getUserDashboard } = this.props
    getUserDashboard(token)
    .then(console.log)
    .catch(console.log)
  }

  /**
   * returns skip button
   */
  getSkipButton () {
    return (
      <View>
        <TouchableOpacity onPress={this.gotoRootPage}>
          <Text style={{ color: "blue" }}> Skip</Text>
        </TouchableOpacity>
      </View>
    )
  }

  /**
   * returns gethelp button
   */
  getHelpButton() {
    return (
      <View style={{ flex: 1, height: 25, paddingLeft: 5 }}>
        <TouchableOpacity onPress={() => { Actions.contactUsPages() }}>
          <Image
            style={{ height: 25, width: 25 }}
            source={require('./components/img/help.png')} />
        </TouchableOpacity>
      </View>
    )
  }

  /**
   * return getwallet button
   */
  getWalletButton() {
    return (
      <View style={{ marginTop: 0 }}>
        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => { Actions.walletPages() }}>
          <Image style={{ height: 20, width: 20 }} source={require('./components/img/wallet.png')} />
          <Text style={{ marginLeft: 3, fontSize: 15, color: '#ffffff' }} >Withdrawal</Text>
        </TouchableOpacity>
      </View>
    )
  }

  /**
   * return skiptopages button
   */
  getSkipToPagesButton() {
    return (
      <View style={{ justifyContent: 'center', height: 25, flexDirection: 'row' }}>
        <TouchableOpacity style={{ flexWrap: 'wrap' }} onPress={() => { Actions.mainPages() }}>
          <Text style={{ paddingLeft: 5, color: '#ffffff', fontSize: 16 }} >Skip</Text>
        </TouchableOpacity>
      </View>
    )
  }

  /**
   * open up app
   */
  gotoRootPage = () => {
    const {setFirstTimeUser} = this.props
    setFirstTimeUser()
    .then(() => {
      Actions.loginPages()
    })
  }

  getRoutes = () => {
    return (
      <Router
        navigationBarStyle={styles.navBar}
        titleStyle={styles.navTitle}
        barButtonTextStyle={styles.barButtonTextStyle}
        barButtonIconStyle={styles.barButtonIconStyle}
      >
        <Scene
          key="Pager"
          component={Pager}
          initial={this.state.rootPage === 'Pager' ? true : false}
          renderRightButton={this.getSkipButton.bind(this)}
        />

        <Scene key='mainPages' initial={this.state.rootPage === 'mainPages' ? true : false}>
          <Scene
            key="BottomNav"
            component={BottomNav}
            renderLeftButton={() => this.getHelpButton()}
            renderRightButton={() => this.getWalletButton()}
          />
          <Scene
            key="InvestmentPage"
            component={InvestmentPage}
            renderLeftButton={() => this.getHelpButton()}
            renderRightButton={() => this.getSkipToPagesButton()}
          />
          <Scene
            key="InvestmentDetailsPage"
            component={InvestmentDetailsPage}
            renderRightButton={() => this.getSkipToPagesButton()}
          />
          <Scene
            key="InvestmentPage3"
            component={InvestmentPage3}
            renderRightButton={() => this.getSkipToPagesButton()}
          />
          <Scene key="Webview" component={Webview} />
          <Scene key="EarningNameOfDeposit" component={EarningNameOfDeposit} />
        </Scene>

        <Scene
          key="verifyPin"
          type={ActionConst.RESET}
          initial={this.state.rootPage === 'verifyPin' ? true : false}
          renderRightButton={() => this.getSkipToPagesButton()}
        >
          <Scene
            key="pin"
            component={Pincode}
            renderLeftButton={() => this.getHelpButton()}
          />
        </Scene>

        <Scene key="loginPages" initial={this.state.rootPage === 'loginPages' ? true : false} type={ActionConst.RESET} >
          <Scene key="login" component={LoginPage} renderLeftButton={() => this.getHelpButton()} />
          <Scene key="forgotPassword" component={ForgotPassword} />
          <Scene key="ValidateCode" component={Validatecode} />
          <Scene key="Terms" component={TermsAndCondition} />
        </Scene>



        <Scene key="createnewpassword" type={ActionConst.RESET} >
          <Scene key="CreatenewPassword" component={CreatenewPassword} renderLeftButton={() => this.getHelpButton()} />
        </Scene>

        <Scene key="changePin" type={ActionConst.RESET} initial={this.state.rootPage === 'changePin' ? true : false} >
          <Scene key="changepin" component={ChangePincode} renderLeftButton={() => this.getHelpButton()} />
        </Scene>

        <Scene key="InvestmentPages" >
          <Scene
            key="InvestmentApproach"
            component={InvestmentApproach}
            renderLeftButton={() => this.getHelpButton()}
            renderRightButton={() => this.getSkipToPagesButton()}
          />
          <Scene
            key="InvestmentPage"
            component={InvestmentPage}
            renderLeftButton={() => this.getHelpButton()}
            renderRightButton={() => this.getSkipToPagesButton()}
          />
          <Scene
            key="InvestmentDetailsPage"
            component={InvestmentDetailsPage}
            renderRightButton={() => this.getSkipToPagesButton()}
          />
          <Scene
            key="InvestmentPage3"
            component={InvestmentPage3}
            renderRightButton={() => this.getSkipToPagesButton()}
          />
          <Scene key="Webview" component={Webview} />
        </Scene>


        <Scene key="walletPages" component={WalletPage} renderRightButton={() => this.getWalletButton()} />
        <Scene key="contactUsPages" component={About} renderLeftButton={() => this.getHelpButton()} />

      </Router>
    )
  }

  render() {
    const renderedComponent = () => this.state.loading ? <Text> Loading </Text> : this.getRoutes()
    return (
      renderedComponent()
    )
  }
}

// Styles used for View adjustment
const styles = {
  navBar: {
    flex: 1,
    borderBottomWidth: 0,
    backgroundColor: 'rgba(255, 255, 255, 0)' // changing navbar color
  },
  navTitle: {
    color: 'white', // changing navbar title color
  },
  barButtonTextStyle: {
    color: '#FFFFFF'
  },
  barButtonIconStyle: {
    tintColor: 'rgb(255,255,255)',
    alignSelf: 'flex-start'
  }
}

const mapStateToProps = ({user: {userData, firstTimeUser}}) => ({userData, firstTimeUser})

const mapDispatchToProps = ({
  getUserData,
  getFirstTimeUser,
  setFirstTimeUser,
  getUserDashboard
})

export default connect(mapStateToProps, mapDispatchToProps)(RouterComponent)
